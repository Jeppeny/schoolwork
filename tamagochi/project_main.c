/*
 * Project members: Jesper Nyman, Kalle Tapio ja Tommi V�is�nen
 *
 * We mostly worked on the project together. Jesper did most of the coding (since he had the sensortag most of the time), but since we always
 * worked on the code together (either at the university or remotely) everyone participated pretty much equally.
 *
 *
 */




/* C Standard library */
#include <stdio.h>
#include <string.h>

/* XDCtools files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/drivers/UART.h>

/* Board Header files */
#include "Board.h"
#include "wireless/comm_lib.h"
#include "sensors/opt3001.h"
#include "sensors/tmp007.h"
#include "sensors/mpu9250.h"
#include "buzzer.h"

#define SONG_LENGTH 66

/* Task */
#define STACKSIZE 2048
Char sensorTaskStack[STACKSIZE];
Char uartTaskStack[STACKSIZE];
Char commTaskStack[STACKSIZE];
Char sendTaskStack[STACKSIZE];

void buzzerWarn();
void buzzerBeep();
void buzzerChangeState();

/* tilakoneiden tilat */

// sensoreiden luku
enum dataState {OFF=1, ON};
enum dataState dataState = OFF;

enum state {WAITING=1, DATA_READY, BUZZER_WARN, BUZZER_BEEP, BUZZER_STATE};
enum state programState = WAITING;

//globaalit muuttujat
double ambientLight = -1000.0;
double temp = -1000.0;


int pet = 0;
int feed = 0;
int exercise = 0;


float ax, ay, az, gx, gy, gz;
float time = 0.0;


/*
 * timings and notes for song,
 * code for song from:
 * https://gist.github.com/andrewjbennett/f5c1e632cd38a3e923b4d6cb3c2f35bd
 */

float timings[] = {
                   1.0, 1.0, 1.0, 0.75, 0.25, 1.0, 0.75, 0.25, 2.0, 1.0, 1.0, 1.0, 0.75, 0.25, 1.0,
                   0.75, 0.25, 2.0, 1.0, 0.75, 0.25, 1.0, 0.75, 0.25, 0.25, 0.25, 0.5, 0.5, 1.0, 0.75,
                   0.25, 0.25, 0.25, 0.5, 0.5, 1.0, 0.75, 0.25, 1.0, 0.75, 0.25, 2.0, 1.0, 0.75, 0.25,
                   1.0, 0.75, 0.25, 0.25, 0.25, 0.5, 0.5, 1.0, 0.75, 0.25, 0.25, 0.25, 0.5, 0.5, 1.0,
                   0.75, 0.25, 1.0, 0.75, 0.25, 2.0
                   };

uint16_t notes[] = {
                    440, 440, 440, 349, 523, 440, 349, 523, 440, 659, 659, 659, 698, 523, 415, 349, 523,
                    440, 880, 440, 440, 880, 830, 784, 739, 659, 698, 466, 622, 587, 554, 523, 493, 523,
                    349, 415, 349, 440, 523, 440, 523, 659, 880, 440, 440, 880, 830, 784, 739, 659, 698,
                    466, 622, 587, 554, 523, 493, 523, 349, 415, 349, 523, 440, 349, 523, 440
                    };

uint16_t upto = 0;


// button RTOS global variables
static PIN_Handle buttonHandle;
static PIN_State buttonState;
static PIN_Handle ledHandle;
static PIN_State ledState;

// MPU power pin global variables
static PIN_Handle hMpuPin;
static PIN_State  MpuPinState;

// buzzer variables
static PIN_Handle buzzerHandle;
static PIN_State buzzState;

/* pin configs */

PIN_Config buttonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE
};

PIN_Config buzzerConfig[] = {
  Board_BUZZER | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
  PIN_TERMINATE
};

PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
   PIN_TERMINATE
};

static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// Own i2c interface for mpu
static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};



void buttonFxn(PIN_Handle buttonHandle, PIN_Id pinId) {

    // tamagotchi starts/stops sending data to backend when button is pressed
    if (dataState == OFF) dataState = ON;
    else if (dataState == ON) dataState = OFF;
    programState = BUZZER_STATE;

    // led on/off when button pressed
    uint_t pinValue = PIN_getOutputValue( Board_LED1 );
    pinValue = !pinValue;
    PIN_setOutputValue( ledHandle, Board_LED1, pinValue );

}



/* Task Functions */
Void uartTaskFxn(UArg arg0, UArg arg1) {

    // we are not using uart but left this here
    /*
    UART_Handle uart;
    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_TEXT;
    uartParams.readDataMode = UART_DATA_TEXT;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.readMode = UART_MODE_BLOCKING;
    uartParams.baudRate = 9600;
    uartParams.dataLength = UART_LEN_8;
    uartParams.parityType = UART_PAR_NONE;
    uartParams.stopBits = UART_STOP_ONE;
    uart = UART_open(Board_UART0, &uartParams);
    */


    while (1) {

        switch(programState) {

        case (DATA_READY):

                // Tamagotchi gets pet if temperature is high enough
                if (temp >= 35) {
                    pet = 1;
                }
                else if (temp >= 50) {
                    pet = 2;
                }

                // Our tamagotchi is a plant so it gets fed from bright light
                if (ambientLight >= 100) {
                    feed = 1;
                }
                else if (ambientLight >= 500) {
                    feed = 2;
                }

                // tamagotchi gets exercise if it's moved fast enough along y axis
                if (((ay > 0.4) || (ay < -0.4)) && ((ax < 0.2) && (ax > -0.2))) {
                    exercise = 1;
                    buzzerBeep();
                }
                programState = WAITING;
                break;
        case (BUZZER_BEEP):
                buzzerBeep();
                programState = WAITING;
                break;
        case (BUZZER_STATE):
                buzzerChangeState();
                programState = WAITING;
                break;
        case (BUZZER_WARN):
                buzzerWarn();
                programState = WAITING;
                break;
        default:
                break;
        }

        // sleep 100 ms
        Task_sleep(100000 / Clock_tickPeriod);
    }

}


Void commTaskFxn(UArg arg0, UArg arg1) {

   char payload[16]; // message buffer
   uint16_t senderAddr;
   int32_t result = StartReceive6LoWPAN();
   if(result != true) {
      System_abort("Wireless receive start failed");
   }

   // loop for receiving messages
   while (true) {

        if (GetRXFlag()) {
           memset(payload,0,16);
           Receive6LoWPAN(&senderAddr, payload, 16);

           // if beep is received, we play the warning sound (song)
           if (strncmp(payload, "2050,BEEP", 9)) {
               programState = BUZZER_WARN;
           }
        }
   }
}

Void sendDataFxn(UArg arg0, UArg arg1) {

    while (1) {
        // only send if a value is above 0
        if (dataState == ON && (feed + exercise + pet != 0)) {
            char payload[16];
            sprintf(payload, "ACTIVATE:%d;%d;%d", feed, exercise, pet);
            Send6LoWPAN(IEEE80154_SERVER_ADDR, payload, strlen(payload));
            pet = 0;
            feed = 0;
            exercise = 0;
            Task_sleep(10000 / Clock_tickPeriod);
            StartReceive6LoWPAN();
        }
        //send every 2 seconds
        Task_sleep(2000000 / Clock_tickPeriod);
    }

}


Void sensorTaskFxn(UArg arg0, UArg arg1) {

    // i2c variables for opt ant tmp sensors
    I2C_Handle i2c;
    I2C_Params i2cParams;

    // i2c variables for mpu sensor
    I2C_Handle i2cMPU;
    I2C_Params i2cMPUParams;

    // initialize MPU i2c
    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;

    // MPU power on
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // Wait 100ms for the MPU sensor to power up
    Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();

    // MPU open i2c
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }

    // MPU setup and calibration
    System_printf("MPU9250: Setup and calibration...\n");
    System_flush();
    mpu9250_setup(&i2cMPU);
    System_printf("MPU9250: Setup and calibration OK\n");
    System_flush();

    I2C_close(i2cMPU);


    // initialize i2c for other two sensors
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;

    // open i2c for other sensors
    i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL) {
       System_abort("Error Initializing I2C\n");
    }

    // setup other sensors
    Task_sleep(100000 / Clock_tickPeriod);
    opt3001_setup(&i2c);
    tmp007_setup(&i2c);

    I2C_close(i2c);


    while (1) {

        if (programState == WAITING) {

            /* read data to global variables */

            // tmp & opt
            if ((int)(time * 10) % 10 == 0) {
                i2c = I2C_open(Board_I2C_TMP, &i2cParams);
                if (i2c == NULL) {
                   System_abort("Error Initializing I2C\n");
                }
                ambientLight = opt3001_get_data(&i2c);
                temp = tmp007_get_data(&i2c);
                I2C_close(i2c);
            }
            // mpu
            i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
            if (i2cMPU == NULL) {
                System_abort("Error Initializing I2CMPU\n");
            }
            mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
            I2C_close(i2cMPU);
            time += 0.1;
            programState = DATA_READY;
        }
        // sleep 100 ms
        Task_sleep(100000 / Clock_tickPeriod);
        }
}


void buzzerWarn() {

    // loop for playing song
    while (upto < SONG_LENGTH) {
        buzzerOpen(buzzerHandle);
        buzzerSetFrequency(notes[upto]*2);
        Task_sleep(200000 * timings[upto] / Clock_tickPeriod);
        buzzerClose();
        Task_sleep(200000 * 0.10 / Clock_tickPeriod);
        upto++;
    }
    upto = 0;
}

void buzzerBeep() {
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(4000);
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerSetFrequency(2000);
    Task_sleep(50000 / Clock_tickPeriod);
    buzzerClose();
}

void buzzerChangeState() {
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1000);
    Task_sleep(300000 / Clock_tickPeriod);
    buzzerClose();
}

Int main(void) {

    // Task variables
    Task_Handle sensorTaskHandle;
    Task_Params sensorTaskParams;

    Task_Handle uartTaskHandle;
    Task_Params uartTaskParams;

    Task_Params commTaskParams;
    Task_Handle commTask;

    Task_Params sendDataParams;
    Task_Handle sendDataTask;

    // Initialize board
    Board_initGeneral();

    // initialize 6lowpan
    Init6LoWPAN();
    
    // init i2c
    Board_initI2C();

    // init uart
    Board_initUART();

    // pins
    buttonHandle = PIN_open(&buttonState, buttonConfig);
    if(!buttonHandle) {
       System_abort("Error initializing button pins\n");
    }
    ledHandle = PIN_open(&ledState, ledConfig);
    if(!ledHandle) {
       System_abort("Error initializing LED pins\n");
    }
    // mpu power pin
    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
        System_abort("Pin open failed!");
    }

    // button interrupt
    if (PIN_registerIntCb(buttonHandle, &buttonFxn) != 0) {
       System_abort("Error registering button callback function");
    }

    buzzerHandle = PIN_open(&buzzState, buzzerConfig);
    if (buzzerHandle == NULL) {
      System_abort("Pin open failed!");
    }


    /* Tasks */


    // sensortask
    Task_Params_init(&sensorTaskParams);
    sensorTaskParams.stackSize = STACKSIZE;
    sensorTaskParams.stack = &sensorTaskStack;
    sensorTaskParams.priority=2;
    sensorTaskHandle = Task_create(sensorTaskFxn, &sensorTaskParams, NULL);
    if (sensorTaskHandle == NULL) {
        System_abort("Task create failed!");
    }

    // uarttask
    Task_Params_init(&uartTaskParams);
    uartTaskParams.stackSize = STACKSIZE;
    uartTaskParams.stack = &uartTaskStack;
    uartTaskParams.priority=2;
    uartTaskHandle = Task_create(uartTaskFxn, &uartTaskParams, NULL);
    if (uartTaskHandle == NULL) {
        System_abort("Task create failed!");
    }

    // communication task
    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority = 1; // Important to set the priority to 1
    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
        System_abort("Task create failed!");
    }

    // sendDataTask
    Task_Params_init(&sendDataParams);
    sendDataParams.stackSize = STACKSIZE;
    sendDataParams.stack = &sendTaskStack;
    sendDataParams.priority=2;
    sendDataTask = Task_create(sendDataFxn, &sendDataParams, NULL);
    if (sendDataTask == NULL) {
        System_abort("Task create failed!");
    }



    /* Sanity check */
    System_printf("Hello world!\n");
    System_flush();

    /* Start BIOS */
    BIOS_start();

    return (0);
}
