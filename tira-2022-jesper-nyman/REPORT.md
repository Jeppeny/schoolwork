# Kurssitehtävien oppima ja raportit

Kirjoita jokaisesta alla olevasta kurssitehtävästä parilla lauseella miten tehtävän tekeminen sujui ja mitä siitä opit.

Jos tehtävässä pyydetään **raportoimaan** jotain, kirjoita myös nämä raportit tähän dokumenttiin.

## 00-init
Tehtävä sujui hyvin. Aluksi vscoden ja java eivät toimineet oikein mutta ongelma ratkesi lopulta.

## 01-arrays
Tehtävä sujui myös hyvin. Olin kuullut ennen erilaisista lajittelualgoritmeista ja sellaisen toteuttaminen oli oikein mukavaa!

## 02-mode
Tässä tehtävässä oli hieman vaikeuksia, mutta sain lopulta funktion toimimaan oikein. Aikakompleksisuus vaikuttaisi olevan noin O(n^2), johtuen sortista sekä for loopista.

![Time complexity chart](timecomplexity.png)

## 03-draw
Tehtävä oli mielestäni helpompi kuin edellinen. Mielestäni graafinen käyttöliittymä ohjelman toiminnan visualisointiin on loistava idea! Tehtävä auttoi ymmärtämään kuinka massiivisesti ohjelman tehokkuus voi parantua suhteellisen yksinkertaisella optimoinnilla.

## 04-1-stack
Tehtävä meni hyvin. Hieman vaikeuksia alussa tuotti se, että oli päässyt hieman unohtumaan osa javan perusasioista kuten konstruktoreiden toiminta. Kertasin hieman ohjelmointi 2 -kurssin asioita ja luin tehtävänannon huolella, jonka jälkeen tehtävän teko onnistui.

PS. Huomasin juuri ennen palautusta, että harvoin testi "pushpopstacktest" epäonnistuu. Yritin etsiä koodistani vikaa, mutten löytänyt. Mielestäni testi yrittää ehkä luoda 0:n pituisen testarrayn ja sitten olettaa, että pinossa pitäisi olla jotain?

![Failed test](failedtest.png)

## 04-2-queue
Tehtävä meni hyvin ja siinä pystyi hyödyntämään edeltävässä tehtävässä opittuja asioita. Hieman hämmennystä aiheutti head- ja tail- indeksien käyttäytyminen listaan lisätessä ja poistaessa, mutta luentovideoita katsomalla ja kynällä sekä paperilla hahmoittelemalla ongelmat ratkesivat.

## 04-3-linkedlist
Linkitetyn listan toiminnan hahmottaminen sujui yllättävän hyvin, sillä sen toimintaa oli sivuttu tietokonejärjestelmät-kurssin tehtävissä, sekä olin katsonut aiheeseen liittyvät luennot. Muutama bugi ohjelmassa aluksi oli, mutta niiden korjaaminen oli suhteellisen helppoa.

## 05-binsearch
Tehtävä oli nopea tehdä, sillä puolitushaun idea oli helppo hahmottaa ja liveluennolla tehdystä esimerkistä sai hyvin vinkkejä. Tehtävästä opin miten suhteellisen yksinkertaista ongelmaa kuten hakualgoritmia saa nopeutettua huomattavasti.

## 05-invoices
Tehtävä sujui hyvin ja live-luentojen demosta oli apua. Päätin toteuttaa quicksortin, sillä se vaikutti paremmalta vaihtoehdolta esimerkiksi heap sortiin verratessa.

## 67-phonebook

Tähän tehtävään liittyy raportti! Lue ohjeet!

## Valinnaiset tehtävät

Jos teit jotain valinnaisia tehtäviä, mainitse se täällä että ne tulevat varmasti arvioiduksi.

# Yleistä koko kurssista ja kurssin tehtävistä

Yleistä palautetta ja kehitysehdotuksia, kiitos!