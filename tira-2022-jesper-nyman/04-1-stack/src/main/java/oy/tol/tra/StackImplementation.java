package oy.tol.tra;

/**
 * An implementation of the StackInterface.
 */
public class StackImplementation<E> implements StackInterface<E> {

   // Do not use constant values in code, e.g. 10. Instead, define a constant for that. For example:
   // private static final int MY_CONSTANT_VARIABLE = 10;
   private static final int CAPACITY_CONSTANT = 10;
   private Object [] itemArray;
   private int capacity = 0;
   private int currentIndex = -1;


   /**
    * Allocates a stack with a default capacity.
    * @throws StackAllocationException
    */
   public StackImplementation() throws StackAllocationException {
      capacity = CAPACITY_CONSTANT;
      itemArray = new Object[CAPACITY_CONSTANT];
   }

   /**
    * @param capacity The capacity of the stack.
    * @throws StackAllocationException If cannot allocate room for the internal array.
    */
   public StackImplementation(int capacity) throws StackAllocationException {
      if (capacity < 2) {
         throw new StackAllocationException("Array size too small!");
      }
      try {
         itemArray = new Object[capacity];
         this.capacity = capacity;
         currentIndex = -1;
      } catch (Exception javaException) {
         throw new StackAllocationException("Java exception!");
      }

   }

   @Override
   public int capacity() {
      return capacity;
   }

   public void reallocate(int newCapacity) {
      Object [] newItemArray = new Object[newCapacity];
      int index = 0;
      for (Object item : this.itemArray) {
         newItemArray[index++] = item;
         this.capacity = newCapacity;
         this.itemArray = newItemArray;
      }
   }

   @Override
   public void push(E element) throws StackAllocationException, NullPointerException {
      if (element.equals(null)) {
         throw new NullPointerException();
      }
      if (currentIndex == itemArray.length-1) {
         try {
            reallocate(2 * itemArray.length);
         } catch (Exception e) {
            throw new StackAllocationException("No additional room could be allocated to stack!");
         }
      }
      currentIndex++;
      itemArray[currentIndex] = element;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E pop() throws StackIsEmptyException {
      if (currentIndex == -1) {
         throw new StackIsEmptyException("Cannot pop from empty stack!");
      }
      E element = (E) itemArray[currentIndex];
      itemArray[currentIndex] = null;
      currentIndex--;
      return element;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E peek() throws StackIsEmptyException {
      if (currentIndex == -1) {
         throw new StackIsEmptyException("Stack is empty!");
      }
      return (E) itemArray[currentIndex];
   }

   @Override
   public int size() {
      return currentIndex + 1;
   }

   @Override
   public void clear() {
      itemArray = new Object[capacity];
      currentIndex = -1;
   }

   @Override
   public boolean isEmpty() {
      if (currentIndex == -1) {
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("[");
      for (int i = 0; i <= currentIndex; i++) {
         builder.append(itemArray[i]);
         if (i < currentIndex) {
            builder.append(", ");
         }
      }
      builder.append("]");
      return builder.toString();
   }
}
