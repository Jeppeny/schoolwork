package oy.tol.tra;

/**
 * Uses the StackInterface implementation to check that parentheses in text files
 * match. 
 * <p>
 */
public class ParenthesisChecker {

   private ParenthesisChecker() {
   }

   /**
    * @param stack The stack object used in checking the parentheses from the string.
    * @param fromString A string containing parentheses to check if it is valid.
    * @return Returns the number of parentheses found from the input in total (both opening and closing).
    * @throws ParenthesesException if the parentheses did not match as intended.
    * @throws StackAllocationException If the stack cannot be allocated or reallocated if necessary.
    */
    public static int checkParentheses(StackInterface<Character> stack, String fromString) throws ParenthesesException {

      String checkForStart = "([{";
      String checkForEnd = ")]}";
      
      int amountOfParentheses = 0;
      
      for (int i = 0; i < fromString.length(); i++) {
         char c = fromString.charAt(i);
         if (c == '(' || c == '[' || c == '{') {
            stack.push(c);
            amountOfParentheses++;
         }
         else if (c == ')' || c == ']' || c == '}') {
            try {
               char latest = stack.pop();
               amountOfParentheses++;
               if (checkForStart.indexOf(latest) != checkForEnd.indexOf(c)) {
                  throw new ParenthesesException("Parenthesis don't match!", -3);
               }
            } catch (Exception StackIsEmptyException) {
               throw new ParenthesesException("There are too many closing parentheses!", -1);
            }
         }
      }

      if (!stack.isEmpty()) {
         throw new ParenthesesException("More opening than closing parentheses!", -2);
      }

      return amountOfParentheses;
   }
}
