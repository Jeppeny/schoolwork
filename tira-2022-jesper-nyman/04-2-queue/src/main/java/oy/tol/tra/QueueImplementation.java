package oy.tol.tra;

public class QueueImplementation<E> implements QueueInterface<E> {

    private static final int CAPACITY_CONSTANT = 10;
    private Object [] itemArray;
    private int capacity = 0;
    private int count = 0;
    private int currentHead = 0;
    private int currentTail = 0;

    public QueueImplementation() throws QueueAllocationException {
        itemArray = new Object[CAPACITY_CONSTANT];
        capacity = CAPACITY_CONSTANT;
    }

    public QueueImplementation(int capacity) throws QueueAllocationException {
        if (capacity < 2) {
            throw new QueueAllocationException("Array size too small!");
         }
         try {
            itemArray = new Object[capacity];
            this.capacity = capacity;
            currentHead = 0;
            currentTail = 0;
         } catch (Exception javaException) {
            throw new QueueAllocationException("Java exception!");
         }
    }

    @Override
    public int capacity() {
        // returns capacity
        return capacity;
    }

    @Override
    public void enqueue(E element) throws QueueAllocationException, NullPointerException {
        if (element.equals(null)) {
            throw new NullPointerException("Element cannot be null!");
        }
        if (count == capacity) {
            try {
                reallocate(capacity * 2);
            } catch (Exception e) {
                throw new QueueAllocationException("Could not allocate more space in queue!");
            }
        }
        if (currentTail >= capacity && currentHead > 0) {
            currentTail = 0;
        }
        itemArray[currentTail++] = element;
        count++; 
    }

    public void reallocate(int newCapacity) {
        Object [] tmpArray = new Object[newCapacity];
        int tmp = count;
        for (int i = 0; i < tmp; i++) {
            tmpArray[i] = dequeue();
        }
        itemArray = tmpArray;
        capacity = newCapacity;
        currentHead = 0;
        currentTail = tmp;
        count = tmp;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E dequeue() throws QueueIsEmptyException {
        if (count == 0) {
            throw new QueueIsEmptyException("Unable to dequeue; queue is empty!");
        }
        E tmp = (E) itemArray[currentHead];
        currentHead++;
        count--;
        if (currentHead >= capacity) {
            currentHead = 0;
        }
        return tmp;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E element() throws QueueIsEmptyException {
        // return element at head
        if (count == 0) {
            throw new QueueIsEmptyException("Queue is empty!");
        }
        E element = (E) itemArray[currentHead];
        return element;
    }

    @Override
    public int size() {
        // return the count
        return count;
    }

    @Override
    public boolean isEmpty() {
        if (count == 0) return true;
        return false;
    }

    @Override
    public void clear() {
        itemArray = new Object[capacity];
        count = 0;
        currentHead = 0;
        currentTail = 0;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int tmpIndex = currentHead;
        int tmpCount = count;
        while (tmpCount > 0) {
            builder.append(itemArray[tmpIndex++]);
            tmpCount--;
            if (tmpCount > 0) builder.append(", ");
            if (tmpIndex > capacity) tmpIndex = 0;
        }
        builder.append("]");
        return builder.toString();
    }
    
}
