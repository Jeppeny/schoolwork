package oy.tol.tra;

public class LinkedListImplementation<E> implements LinkedListInterface<E> {

   private class Node<T> {
      Node(T data) {
         element = data;
         next = null;
      }
      T element;
      Node<T> next;
      @Override
      public String toString() {
         return element.toString();
      }
   }

   private Node<E> head = null;
   private int size = 0;

   @Override
   public void add(E element) throws NullPointerException, LinkedListAllocationException {
      if (null == head) {
         head = new Node<E>(element);
         size++;
      } else {
         Node<E> current = head;
         while (null != current.next) {
            current = current.next;
         }
         current.next = new Node<E>(element);
         size++;
      }
   }

   @Override 
   public void add(int index, E element) throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
      if (index < 0 || index > size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      if (null == element) {
         throw new NullPointerException("Element cannot be null");
      }
      if (0 == index) {
         Node<E> newNode = new Node<E>(element);
         newNode.next = head;
         head = newNode;
         size++;
      } else {
         int counter = 0;
         Node<E> current = head;
         Node<E> previous = null;
         while (counter < index) {
            previous = current;
            current = current.next;
            counter++;
         }
         Node<E> newNode = new Node<E>(element);
         previous.next = newNode;
         newNode.next = current;
         size++;
      }
   }

   @Override
   public boolean remove(E element) throws NullPointerException {
      if (null == element) {
         throw new NullPointerException("Element cannot be null");
      }
      if (null == head) {
         return false;
      }
      if (element.equals(head.element)) {
         head = head.next;
         size--;
      } else {
         int counter = 0;
         Node<E> current = head;
         Node<E> previous = null;
         while (counter < size) {
            if (element.equals(current.element)) {
               previous.next = current.next;
               size--;
               return true;
            }
            previous = current;
            current = current.next;
            counter++;
         }
      }
      return false;
   }

   @Override
   public E remove(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      if (0 == index) {
         head = head.next;
         size--;
      } else {
         int counter = 0;
         Node<E> current = head;
         Node<E> previous = null;
         while (counter < index) {
            previous = current;
            current = current.next;
            counter++;
         }
         Node<E> nodeToReturn = current;
         previous.next = current.next;
         size--;
         return nodeToReturn.element;
      }
      return null;
   }

   @Override
   public E get(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      if (0 == index) {
         return head.element;
      } else {
         int counter = 0;
         Node<E> current = head;
         while (counter < index) {
            current = current.next;
            counter++;
         }
         return current.element;
      }
   }

   @Override
   public int indexOf(E element) throws NullPointerException {
      if (null == element) {
         throw new NullPointerException("Element cannot be null");
      }
      if (null == head) {
         return -1;
      }
      if (element.equals(head.element)) {
         return 0;
      } else {
         int counter = 0;
         Node<E> current = head;
         while (counter < size) {
            if (element.equals(current.element)) {
               return counter;
            }
            current = current.next;
            counter++;
         }
      }
      return -1;
   }

   @Override
   public int size() {
      return size;
   }

   @Override
   public void clear() {
      head = null;
      size = 0;
   }

   @Override
   public void reverse() {
      int counter = 0;
      Node<E> current = head;
      Node<E> previous = null;
      while (counter < size) {
         Node<E> nextNode = current.next;
         current.next = previous;
         previous = current;
         current = nextNode;
         counter++;
      }
      head = previous;
   }

   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("[");
      int counter = 0;
      Node<E> current = head;
      while (counter < size) {
         builder.append(current);
         current = current.next;
         if (counter < size - 1) builder.append(", ");
         counter++;
      }
      builder.append("]");
      return builder.toString();
   }
   
}
