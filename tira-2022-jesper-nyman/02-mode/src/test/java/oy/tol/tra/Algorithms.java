package oy.tol.tra;

public class Algorithms {

    public static <T extends Comparable<T>> void sort(T [] array) {
        int i = array.length-2;
        while (i >= 0) {
           int j = i;
           while (j < array.length-1 && array[j].compareTo(array[j+1]) > 0) {
                swap(array, j, j+1);
                j++;
           }
           i--;
        }
    }

    public static <T> void reverse(T [] array) {
        int i = 0;
        while (i < array.length/2) {
            swap(array, i, array.length-i-1);
            i++;
       }
    }

    public static <T> void swap(T [] array, int first, int second) {
        T temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }


    public static class ModeSearchResult<T> {
        public T theMode = null;
        public int count = -1;
     }
  
    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T [] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        int currentResult = 1;
        T currentMode = null;
        if (array != null) {
            sort(array);
            for (int i = 0; i < array.length-1; i++) {
                if (array[i].compareTo(array[i+1]) == 0) {
                    currentResult++;
                    currentMode = array[i];
                }
                if (i == array.length-2 && currentResult > result.count && currentResult > 1) {
                    result.theMode = currentMode;
                    result.count = currentResult;
                }
                else if (currentResult > result.count && currentResult > 1) {
                    result.theMode = currentMode;
                    result.count = currentResult;
                }
                else {
                    currentMode = null;
                    currentResult = 1;
                }
            }
        }

        return result;
    }
  

}
