package oy.tol.tra;

public class Algorithms {

    public static <T extends Comparable<T>> void sort(T [] array) {
        // toteutus tähän...
        int i = array.length-2;
        while (i >= 0) {
           int j = i;
           while (j < array.length-1 && array[j].compareTo(array[j+1]) > 0) {
                swap(array, j, j+1);
                j++;
           }
           i--;
        }
    }

    public static <T> void reverse(T [] array) {
        // toteutus tähän...
        int i = 0;
        while (i < array.length/2) {
            swap(array, i, array.length-i-1);
            i++;
       }
    }

    public static <T> void swap(T [] array, int first, int second) {
        T temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }
}
