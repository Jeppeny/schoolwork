package oy.tol.tra;

import java.util.function.Predicate;

public class Algorithms {

    public static <T extends Comparable<T>> void sort(T [] array) {
        int i = array.length-2;
        while (i >= 0) {
           int j = i;
           while (j < array.length-1 && array[j].compareTo(array[j+1]) > 0) {
                swap(array, j, j+1);
                j++;
           }
           i--;
        }
    }

    public static <T> void reverse(T [] array) {
        int i = 0;
        while (i < array.length/2) {
            swap(array, i, array.length-i-1);
            i++;
       }
    }

    public static <T> void swap(T [] array, int first, int second) {
        T temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }


    public static class ModeSearchResult<T> {
        public T theMode = null;
        public int count = -1;
     }
  
    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T [] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        int currentResult = 1;
        T currentMode = null;
        if (array != null) {
            // sort the array so that the same values are next to each other
            sort(array);
            for (int i = 0; i < array.length-1; i++) {
                if (array[i].compareTo(array[i+1]) == 0) {
                    currentResult++;
                    currentMode = array[i];
                }
                if (i == array.length-2 && currentResult > result.count && currentResult > 1) {
                    result.theMode = currentMode;
                    result.count = currentResult;
                }
                else if (currentResult > result.count && currentResult > 1) {
                    result.theMode = currentMode;
                    result.count = currentResult;
                }
                else {
                    currentMode = null;
                    currentResult = 1;
                }
            }
        }

        return result;
    }
  

    public static <T> int partitionByRule(T [] array, int count, Predicate<T> rule) {
        int index = count;
        for (int i = 0; i < array.length; i++) {
            if (rule.test(array[i])) {
                index = i;
                break;
            }
        }
        if (index >= count) return count;

        int nextIndex = index + 1;

        while (nextIndex < count) {
            if (!rule.test(array[nextIndex])) {
                swap(array, index, nextIndex);
                index = index +1;
            }
            nextIndex++;
        }
        return index;
    }

    public static <T extends Comparable<T>> int binarySearch(T aValue, T [] fromArray, int fromIndex, int toIndex) {
        if (fromIndex == toIndex) {
            if (fromArray[fromIndex].compareTo(aValue) == 0) {
                return fromIndex;
            } else return -1;
        } else {
            int middle = fromIndex + (toIndex - fromIndex) / 2;
            if (aValue.compareTo(fromArray[middle]) <= 0) {
                return binarySearch(aValue, fromArray, fromIndex, middle);
            } else {
                return binarySearch(aValue, fromArray, middle + 1, toIndex);
            }
        }
    }
    
    public static <T extends Comparable<T>> void fastSort(T [] array) {
        quickSort(array, 0, array.length-1);
    }

    public static <T extends Comparable<T>> void quickSort(T [] array, int low, int high) {
        if (low < high) {
            int partitionIndex = partition(array, low, high);
            quickSort(array, low, partitionIndex-1);
            quickSort(array, partitionIndex+1, high);
        }
    }

    public static <T extends Comparable<T>> int partition(T [] array, int low, int high) {
        T pivotValue = array[high];
        int i = low-1;
        for (int j = low; j < high; j++) {
            if (array[j].compareTo(pivotValue) < 0) {
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i+1, high);
        return i+1;
    }


}
