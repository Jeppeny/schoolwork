package oy.tol.tra;

public class KeyValueHashTable<K extends Comparable<K>, V> implements Dictionary<K, V> {

    private int capacity, size, collisionCount, maxProbingCount, pairsUpdated, count;
    private Pair<K,V>[] array = null;
    private static final double LOAD_FACTOR = 0.65;

    public KeyValueHashTable(int capacity) throws OutOfMemoryError {
        ensureCapacity(capacity);
    }

    public KeyValueHashTable() throws OutOfMemoryError {
        ensureCapacity(20);
    }

    @Override
    public Type getType() {
        return Type.NONE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        // TODO: Implement this!

        if (count == 0) {
            array = (Pair<K,V>[])new Pair[size];
            this.capacity = size;
        } else {
            reallocate(capacity);
        }

    }

    @Override
    public int size() {
        // TODO: Implement this!
        return 0;
    }

    /**
     * Prints out the statistics of the hash table.
     * Here you should print out member variable information which tell something
     * about your implementation.
     * <p>
     * For example, if you implement this using a hash table, update member
     * variables of the class (int counters) in add() whenever a collision
     * happen. Then print this counter value here.
     * You will then see if you have too many collisions. It will tell you that your
     * hash function is not good.
     */
    @Override
    public String getStatus() {
        // TODO: Implement this!
        return null;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        // TODO: Implement this!

        int index = 0;
        int hashModifier = 0;
        int currentProbingCount = 0;
        int collisionCount = 0;
        boolean added = false;

        while (!added) {
            index = indexFor(key, hashModifier);
            if (array[index] == null) {
                array[index] = new Pair<K,V>(key, value);
                added = true;
                this.count++;
            } else if (!array[index].getKey.equals(key)) {
                hashModifier++;
                currentProbingCount++;
                collisionCount++;
            } else {
                array[index]
            }
        }

        return false;
    }

    public int indexFor(K key, int hashModifier) {
        return key.hashCode() + hashModifier & 0x7fffffff % capacity;
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        // TODO: Implement this!
        return null;
    }

    @Override
    @java.lang.SuppressWarnings({"unchecked"})
    public Pair<K,V> [] toSortedArray() {
        // TODO: Implement this!
        return null;
      }

    @Override
    public void compress() throws OutOfMemoryError {
        // TODO: Implement this!
    }
 

}
