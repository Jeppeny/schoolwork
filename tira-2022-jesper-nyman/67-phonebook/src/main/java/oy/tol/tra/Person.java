package oy.tol.tra;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName() {
        return lastName + " " + firstName;
    }

    // TODO: Implement equals(), hashCode() and Comparable interface.

    @Override
    public int hashCode() {
        int hashCode = 7;
        String stringToUse = this.getFullName().replaceAll("\\s+", "");

        for (int i = 0; i < stringToUse.length(); i++) {
            hashCode = hashCode*31 + stringToUse.charAt(i);
        }     

        return hashCode;
    }

    @Override
    public int compareTo(Person p) {
        return this.getFullName().compareTo(p.getFullName());
    }

    public boolean equals(Object p) {
        if (p instanceof Person) {
            return this.getFullName().equals(((Person)p).getFullName());
        }
        return false;
    }
}
