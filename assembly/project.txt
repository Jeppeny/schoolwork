# Alussa kirjoitetaan lukujono 3,5,8 muistiin
# Hox! Lovelacen tarkistin tekee tämän automaattisesti
.pos 0
    irmovq pino, %rsp    # pino-osoitin
    irmovq pino, %rbp    # pinon alkuosoite
    irmovq array,%r11        # muistipaikka 0x700
    irmovq $3,%r12           # 3
    rmmovq %r12,(%r11)
    irmovq $5,%r12           # 5
    rmmovq %r12,8(%r11)
    irmovq $8,%r12           # 8
    rmmovq %r12,16(%r11)
    irmovq $0,%r12           # loppunolla
    rmmovq %r12,24(%r11)
   
   
# Tästä alkaa palautettava koodi
main:
    irmovq $0, %r14         # MOVE 0 TO R14 FOR SUBTRACTION
    irmovq $8, %r10         # 8 FOR MOVING TO NEXT MEMORY ADDRESS
    mrmovq (%r11), %r12     # VALUE FROM MEMORY TO REGISTER R12
    addq %r12, %r11         # NEXT MEMORY ADDRESS
    subq %r14, %r12
    jne checkFibonacci      # IF VALUE NOT 0, CHECK IF FIBONACCI
    irmovq $0, %rax         # IF VALUE IS 0, MOVE 0 TO RAX
    halt                    # AND STOP THE PROGRAM
    
    
checkFibonacci:
    rrmovq %r12, %r8        # MOVE THE VALUE TO CHECK TO R8
    pushq %r8               # PUSH R8 TO STACK FOR MULTIPLICATION
    pushq %r8               # AGAIN TO MULTIPLY BY ITSELF
    call multiplymain       # CALL MULTIPLY
    irmovq $5, %r8          
    pushq %r8
    call multiplymain       # MULTIPLY AGAIN BY 5
    popq %r8                # POP THE RESULT OF THE MULTIPLICATION
    rrmovq %r8, %r9         # MOVE RESULT TO R9
    irmovq $4, %r14         # MOVE 4 TO R14 FOR SUBTRACTION
    addq %r14, %r9          # ADD 4 TO THE RESULT OF MULTIPLICATION
    pushq %r9               # PUSH THE SUM TO STACK
    call isperfectsquare    # CHECK IF PERFECT SQUARE, RESULT TO %RDI
    rrmovq %rdi, %r9        # IF PERFECT SQUARE, THIS WILL BE 1
    subq %r14, %r8          # SUBTRACT 4 FROM THE MULTIPLIED VALUE
    pushq %r8               # PUSH THE RESULT TO STACK
    call isperfectsquare    # CHECK IF PERFECT SQUARE
    rrmovq %rdi, %r8        # MOVE THE RESULT (0 OR 1) BACK TO R8
    xorq %r9, %r8           # CHECK IF R9 OR R8 IS 1
    jg main                 # IF YES, REPEAT
    rrmovq %r12, %rax       # ELSE NOT FIBONACCI, PUSH ORIGINAL VALUE TO R12
    halt                    # AND STOP EXECUTION
    
    
    
.pos 0x100
isperfectsquare:
    popq %rdi
    pushq %rdi
    call sqrtmain
    popq %rsi
    subq %rdi, %rsi
    irmovq $1, %rsi
    irmovq $0, %rdi
    cmove %rsi, %rdi
    ret
    
# DO NOT MODIFY:
#   R12, R11
#
    
.pos 0x300
multiplymain:
    # SOME VALUES FOR COMPARISONS & SUCH
    irmovq $1, %r8
    irmovq $0, %r9
    irmovq $1, %r10
    irmovq $63, %r13        # HOW MANY TIMES DO WE RUN THE LOOP
    
    popq %rdx               # POP VALUES TO MULTIPLY FROM STACK
    popq %rbx
    rrmovq %rdx, %rcx       # MOVE THE FIRST VALUE TO RCX
    andq %r8, %rcx          # COMPARE THE FIRST VALUE WITH 1
    jg multiply             # IF THE FIRST BIT IS ONE WE MOVE TO MULTIPLY
    jmp loop                # ELSE JUMP TO THE LOOP
    
loop:
    rrmovq %rdx, %rcx       # MOVE THE ORIGINAL VALUE TO RCX
    addq %r8, %r8           # SHIFT R8 TO THE LEFT
    addq %r10, %r14         # HOW MANY SHIFTS HAVE HAPPENED STORED IN R14
    andq %r8, %rcx          # CHECK IF SPECIFIC BIT HAS 1
    jg multiply             # IF YES, JUMP TO MULTIPLY
    
    # FOR WHILE LOOP
    subq %r10, %r13         
    jg loop
    halt

multiply:                   # FUNCTION FOR MULTIPLICATION
    rrmovq %r14, %rdi       # NUMBER OF SHIFTS TO RDI
    rrmovq %rbx, %rcx       # SECOND VALUE TO MULTIPLY TO RCX
    subq %r9, %rdi
    jg multiplyLoop         # ON THE 1st TIME THIS IS SKIPPED EVEN IF BIT WAS 1
    addq %rcx, %rax         
    jmp loop

multiplyLoop:               # SHIFT OTHER VALUE TO THE LEFT BASED ON HOW MANY TIMES R8 HAS BEEN SHIFTED
    addq %rcx, %rcx
    subq %r10, %rdi
    jg multiplyLoop
    addq %rcx, %rax
    jmp loop                # RINSE & REPEAT


.pos 0x400
sqrtmain:
    irmovq pino, %rsp
    irmovq pino, %rbp
    irmovq $1, %r11              # STORE NUMBER 1 FOR LOOP IN R11
    irmovq $1, %r13              # BIT = R13
    irmovq $16, %r14             # HOW MANY SHIFTS

lshiftloop:
    addq %r13, %r13         # ADD BIT TO ITSELF (shift to left)
    subq %r11, %r14         # LOOP 16 TIMES
    jg lshiftloop
    
                            # NOW BIT = R13 AND NUM = R12

whileLoopOne:               # WHILE (BIT > NUM) => BIT >>= 2
    rrmovq %r13, %rax
    call rshiftmain         # CALL THE RIGHT SHIFT TWICE
    call rshiftmain
    rrmovq %rax, %r13       # RESULT OF TWO BIT SHIFTS STORED IN R13 AGAIN
    rrmovq %r13, %r9        # BIT IS STORED TEMPORARILY IN R9
    subq %r12, %r9          # IF BIT > NUM DO LOOP AGAIN
    jg whileLoopOne
    irmovq $0, %r9          # CLEAR R9 FOR LATER
    
                            # NOW BIT = R13 AND NUM = R12

whileLoopTwo:
    irmovq $0, %rbx
    irmovq $0, %r9
    addq %r13, %r9
    addq %rcx, %r9
                            # R9 NOW HAS BIT + RES
    rrmovq %r12, %r10       # STORE NUM TEMPORARILY IN R10
    subq %r9, %r10          
    jge true
    jmp false               # CHECK IF NUM >= R9, JUMP TO TRUE OR FALSE
    

true:
    subq %r9, %r12          # NUM -= BIT + RES
    rrmovq %rcx, %rax       # RES TO RAX FOR SHIFT OPERATION
    call rshiftmain
    rrmovq %rax, %rcx       # RESULT OF BIT SHIFT STORED IN RCX AGAIN
    addq %r13, %rcx         # ADD RES >> 1 TO BIT, STORE IT IN RES (RCX)
    
    
    rrmovq %r13, %rax
    call rshiftmain
    call rshiftmain
    rrmovq %rax, %r13
    subq %r13, %rbx
    jne whileLoopTwo
    halt

false:
    rrmovq %rcx, %rax
    call rshiftmain
    rrmovq %rax, %rcx
    rrmovq %r13, %rax
    call rshiftmain
    call rshiftmain
    rrmovq %rax, %r13
    subq %r13, %rbx
    jne whileLoopTwo
    halt

.pos 0x6a0
rshiftmain:
    xorq %rdx, %rdx
    irmovq $1, %rdi
    irmovq $63, %rsi
    jmp rshift
    
rshift:
    addq %rdx, %rdx
    andq %rax, %rax
    jge MSB_zero
    xorq %rdi, %rdx

MSB_zero:
    addq %rax, %rax
    subq %rdi, %rsi
    jne rshift
    rrmovq %rdx, %rax
    irmovq $0, %rdx
    ret
    
.pos 0x700
array:

.pos 0x1000
pino:

