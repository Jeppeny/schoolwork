def tallenna_summat(lista, tiedosto):
    try:
        with open(tiedosto, "w") as kohde:
            for summat in lista:
                kohde.write("{}:{}:{}:{}\n".format(summat[0], summat[1], summat[2], summat[3]))
            
            
            
    except IOError:
        print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui")