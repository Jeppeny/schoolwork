import random
import haravasto as hr
import time
import json
from timeit import default_timer as timer

TILASTOT_TIEDOSTONIMI = "tilastot.json"
GRAFIIKAT_POLKU = "/Users/Jesper/Documents/Programming/Miinaharava/spritet"

pelin_tiedot = {"alkuajat" : [],
"loppuajat" : [],
"lopputulokset" : [],
"kestot" : [],
"miinat" : [],
"koko" : []}

tila = {"kentta" : [],
"kaynnissa" : True}

dimensiot = {"leveys" : 9, 
"korkeus" : 9,
"miina_lkm" : 30}

pelin_kesto = [0, 0]
            
            
def kysy_dimensiot():
    '''
    Kysyy käyttäjältä miinakentän dimensiot
    '''
    
    while True:
        try:
            dimensiot["leveys"] = int(input("Anna kentän leveys: "))
            dimensiot["korkeus"] = int(input("Anna kentän korkeus: "))
            dimensiot["miina_lkm"] = int(input("Anna miinojen lukumäärä: "))
            pelin_tiedot["koko"].append((dimensiot["leveys"], dimensiot["korkeus"]))
            pelin_tiedot["miinat"].append(dimensiot["miina_lkm"])
        except ValueError:
            print("Anna positiivinen kokonaisluku")
        else:
            break




def kasittele_hiiri(x, y, painike, mnappaimet):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä.
    Palauttaa hiiren klikkauksen sijainnin koordinaatteina kentällä.
    """
    hiiripainikkeet = {
                    hr.HIIRI_VASEN: "vasen",
                    hr.HIIRI_OIKEA: "oikea",
                    hr.HIIRI_KESKI: "keski"
                    }
                    
    x = int(x / 40)
    y = int(y / 40)
    if hiiripainikkeet[painike] == "vasen":
        if tila["kentta"][y][x] == "x":
            print("Valitsemasi ruutu sisälsi miinan")
            print("Hävisit pelin!")
            paljasta_miinat()
            pelin_tiedot["loppuajat"].append(time.strftime("%H:%M:%S %d-%m-%Y", time.localtime()))
            pelin_tiedot["lopputulokset"].append("h")
            pelin_kesto[0] = timer()
            time.sleep(2); hr.lopeta()
        else:
            if tila["kentta"][y][x] != " ":
                print("Valitse ruutu jota ei ole vielä tutkittu")
            elif laske_miinat(tila["kentta"], x, y) != 0:
                tila["kentta"][y][x] = laske_miinat(tila["kentta"], x, y)
            else:
                tulvataytto(tila["kentta"], x, y)



def jaljella_olevia():
    '''
    Laskee, onko kentällä jäljellä avaamattomia ruutuja voittamista varten.
    '''
    
    tutkitut_rivit = 0
    
    for i, rivi in enumerate(tila["kentta"]):
        if rivi.count(" ") == 0:
            tutkitut_rivit += 1
    if tutkitut_rivit == i+1:
        print("Onneksi olkoon, voitit pelin!")
        paljasta_miinat()
        pelin_tiedot["loppuajat"].append(time.strftime("%H:%M:%S %d-%m-%Y", time.localtime()))
        pelin_tiedot["lopputulokset"].append("v")
        pelin_kesto[1] = timer()
        time.sleep(2); hr.lopeta()
    



def piirra_kentta():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    
    hr.tyhjaa_ikkuna()
    hr.piirra_tausta()
    hr.aloita_ruutujen_piirto()
    
    for y, rivi in enumerate(tila["kentta"]):
        for x, ruutu in enumerate(rivi):
            sarake = x * 40
            rivi = y * 40
            if tila["kentta"][y][x] == "x":
                hr.lisaa_piirrettava_ruutu(" ", sarake, rivi)
            elif tila["kentta"][y][x] == "X":
                hr.lisaa_piirrettava_ruutu("x", sarake, rivi)
            else:
                hr.lisaa_piirrettava_ruutu(tila["kentta"][y][x], sarake, rivi)

    hr.piirra_ruudut()




def paljasta_miinat():
    '''
    Paljastaa kaikki kentällä olleet miinat pelin päättyessä.
    '''
    for y, rivi in enumerate(tila["kentta"]):
        for x, ruutu in enumerate(rivi):
            if ruutu == "x":
                tila["kentta"][y][x] = "X"




def tulvataytto(kentta, aloitus_x, aloitus_y):
    """
    Aloittaa täytön x, y pisteestä ja täyttää miinakentällä aloituspisteeseen 
    yhteydessä olevat miinattomat ruudut.
    """
    
    rivit_lkm = len(tila["kentta"])
    sarakkeet_lkm = len(tila["kentta"][0])
    
    lista = [(aloitus_y, aloitus_x)]

    while len(lista) > 0:
        y, x = lista.pop(-1)
        tila["kentta"][y][x] = "0"
        for y1, rivi in enumerate(tila["kentta"]):
            if y1 == y or y1 == y - 1 or y1 == y + 1:
                for x1, ruutu in enumerate(rivi):
                    if x1 == x or x1 == x - 1 or x1 == x + 1:
                        if ruutu == " " and sarakkeet_lkm >= y1 >= 0 and rivit_lkm >= x1 >= 0 and laske_miinat(tila["kentta"], x1, y1) == 0:
                            lista.append((y1, x1))
                        elif laske_miinat(tila["kentta"], x1, y1) >= 1:
                            tila["kentta"][y1][x1] = str(laske_miinat(tila["kentta"], x1, y1))

    







def laske_miinat(kentta, x, y):

    """
    Laskee ruudun ympärillä olevien miinojen lukumäärän
    """
    
    rivi_lkm = len(tila["kentta"])
    sarake_lkm = len(tila["kentta"][0])
    miina_lkm = 0
    
    for y1, rivi in enumerate(tila["kentta"]):
        if y1 == y or y1 == y + 1 or y1 == y - 1:
            for x1, ruutu in enumerate(rivi):
                if x1 == x or x1 == x - 1 or x1 == x + 1:
                    if ruutu == 'x' and sarake_lkm >= y1 >= 0 and rivi_lkm >= x1 >= 0:
                        miina_lkm += 1
                        
    return miina_lkm
    
    
def luo_kentta(leveys, korkeus, miinat_lkm):
    '''
    Luo pelaajan toivomuksien kokoisen miinakentän peliä varten
    '''
    
    tila["kentta"] = []
    
    for rivi in range(korkeus):
        tila["kentta"].append([])
        for sarake in range(leveys):
            tila["kentta"][-1].append(" ")
            
    jaljella = []
    for x in range(leveys):
        for y in range(korkeus):
            jaljella.append((x, y))

    miinoita(tila["kentta"], jaljella, dimensiot["miina_lkm"])




def main():
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """

    hr.lataa_kuvat(GRAFIIKAT_POLKU)
    hr.luo_ikkuna(dimensiot["leveys"] * 40, dimensiot["korkeus"] * 40)
    hr.aseta_hiiri_kasittelija(kasittele_hiiri)
    hr.aseta_piirto_kasittelija(piirra_kentta)
    hr.aseta_toistuva_kasittelija(jaljella_olevia, 1)
    hr.aloita()
    



def tallenna_tiedostoon(tiedosto):
    
    pelin_tiedot["kestot"].append(pelin_kesto[0]-pelin_kesto[1])
    
    try:
        with open(tiedosto, "w") as kohde:
            json.dump(pelin_tiedot, kohde)
    except IOError:
        print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui.")




def lataa_tiedot(tiedosto):
    try:
        with open(tiedosto) as lahde:
            pelin_tiedot = json.load(lahde)
    except (IOError, json.JSONDecodeError):
        print("Tiedoston avaaminen ei onnistunut. Aloitetaan tyhjällä kokoelmalla")
    
    
    
    
def tulosta_tiedot(tiedosto):
    '''
    Tulostaa tilastotietoja pelatuista peleistä.
    '''
    
    for i, alkuaika in enumerate(pelin_tiedot["alkuajat"]):
        print("Pelin {} alkuaika: {}".format(i+1, alkuaika))
    for i, loppuaika in enumerate(pelin_tiedot["loppuajat"]):
        print("Pelin {} loppumisaika: {}".format(i+1, loppuaika))
    for i, kesto in enumerate(pelin_tiedot["kestot"]):
        print("Peli {} kesti {:2} sekuntia".format(i+1, kesto))
    for i, koko in enumerate(pelin_tiedot["koko"]):
        leveys, korkeus = koko
        print("Pelin {} pelikentän koko oli {}x{}".format(i+1, leveys, korkeus))
    for i, miinat in enumerate(pelin_tiedot["miinat"]):
        print("Pelissä {} oli {} miinaa".format(i+1, miinat))
    for i, tulos in enumerate(pelin_tiedot["lopputulokset"]):
        if tulos == "v":
            print("Pelin {} tulos oli voitto".format(i+1))
        elif tulos == "h":
            print("Pelin {} tulos oli häviö".format(i+1))

    


if __name__ == "__main__":
    
    
    print("Tervetuloa Miinantallaajaan!")
    print()
    print("Sinulla on kolme valintaa:")
    print("(A)loita uusi peli")
    print("(L)opeta")
    print("(K)atso tilastot")
    
    
    try:
        while True:
            valinta = input("Tee valintasi: ")
            if valinta == "a":
                kysy_dimensiot()
                luo_kentta(dimensiot["leveys"], dimensiot["korkeus"], dimensiot["miina_lkm"])
                pelin_tiedot["alkuajat"].append(time.strftime("%H:%M:%S %d-%m-%Y", time.localtime()))
                pelin_kesto.append(timer())
                main()
                tallenna_tiedostoon("tulokset.json")
            elif valinta == "l":
                print()
                print()
                print("Kiitos pelaamisesta")
                print()
                print("░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░")
                print("░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░")
                print("░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░")
                print("░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░")
                print("░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░")
                print("█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█")
                print("█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█")
                print("░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░")
                print("░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░")
                print("░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░")
                print("░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░")
                print("░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░")
                print("░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░")
                print("░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░")
                print("░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░")
                print()
                print("Näkemisiin!")
                break
            elif valinta == "k":
                lataa_tiedot(TILASTOT_TIEDOSTONIMI)
                tulosta_tiedot(TILASTOT_TIEDOSTONIMI)
            else:
                print("Valitsemaasi toimintoa ei ole olemassa")
    except KeyboardInterrupt:
            print()
            print()
            print("Kiitos pelaamisesta")
            print()
            print("░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░")
            print("░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░")
            print("░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░")
            print("░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░")
            print("░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░")
            print("█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█")
            print("█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█")
            print("░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░")
            print("░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░")
            print("░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░")
            print("░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░")
            print("░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░")
            print("░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░")
            print("░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░")
            print("░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░")
            print()
            print("Näkemisiin!")
            