import haravasto as h

tila = {
    "kentta": []
}

def piirra_kentta():
    
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    
    h.tyhjaa_ikkuna()
    h.piirra_tausta()
    h.aloita_ruutujen_piirto()
    
    for y, rivi in enumerate(tila['kentta']):
        for x, ruutu in enumerate(rivi):
            sarake = x * 40
            rivi = y * 40
            if ruutu == 'x':
                h.lisaa_piirrettava_ruutu("x", sarake, rivi)
            elif ruutu == '0':
                h.lisaa_piirrettava_ruutu("0", sarake, rivi)
            else:
                h.lisaa_piirrettava_ruutu(" ", sarake, rivi)

    h.piirra_ruudut()
    
    
    
def main(planeetta):
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """
    tila["kentta"] = planeetta
    leveys = len(planeetta[0])
    korkeus = len(planeetta)
    
    h.lataa_kuvat('/Users/Jesper/Documents/Programming/Miinaharava/spritet')
    h.luo_ikkuna(leveys * 40, korkeus * 40)
    h.aseta_piirto_kasittelija(piirra_kentta)
    h.aloita()
    
    
def tulvataytto(planeetta, aloitus_x, aloitus_y):
    """
    Merkitsee planeetalla olevat tuntemattomat alueet turvalliseksi siten, että
    täyttö aloitetaan annetusta x, y -pisteestä.
    """
    
    rivit_lkm = len(planeetta)
    sarakkeet_lkm = len(planeetta[0])
    
    lista = [(aloitus_y, aloitus_x)]

    if planeetta[aloitus_y][aloitus_x] == "x":
        pass
    else:
        while len(lista) > 0:
            y, x = lista.pop(-1)
            planeetta[y][x] = "0"
            for y1, rivi in enumerate(planeetta):
                if y1 in range(y-1, y+2):
                    for x1, ruutu in enumerate(rivi):
                        if x1 in range(x-1, x+2):
                            if ruutu == " " and sarakkeet_lkm >= y1 >= 0 and rivit_lkm >= x1 >= 0:
                                lista.append((y1, x1))


planeetta = [
    [" ", " ", " ", "x", " ", " ", " ", " ", " ", " ", " ", "x", " "], 
    [" ", " ", "x", "x", " ", " ", " ", "x", " ", " ", " ", "x", " "], 
    [" ", "x", "x", " ", " ", " ", " ", "x", " ", " ", "x", "x", " "], 
    ["x", "x", "x", "x", "x", " ", " ", "x", " ", "x", " ", " ", " "], 
    ["x", "x", "x", "x", " ", " ", " ", " ", "x", " ", "x", " ", " "], 
    [" ", " ", "x", " ", " ", " ", " ", " ", " ", "x", " ", " ", " "]
]

tulvataytto(planeetta, 5, 3)

print(planeetta[0])
print(planeetta[1])
print(planeetta[2])
print(planeetta[3])
print(planeetta[4])
print(planeetta[5])


if __name__ == "__main__":
    main(planeetta)