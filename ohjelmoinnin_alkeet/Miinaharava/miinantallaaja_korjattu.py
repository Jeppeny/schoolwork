import random
import haravasto as hr
import time
import json
from timeit import default_timer as timer

TILASTOT_TIEDOSTONIMI = "tilastot.json"
GRAFIIKAT_POLKU = "/Users/Jesper/Documents/Programming/Miinaharava/spritet"


dimensiot = {"leveys" : 9, 
"korkeus" : 9,
"miina_lkm" : 30}

tila = {"kentta" : [],
"running" : True}


pelin_tiedot = {"alkuaika" : [],
"loppuaika" : [],
"lopputulos" : [],
"kesto" : [],
"miinat" : [],
"koko" : []}

pelin_kesto = [0, 0]



def tallenna_tiedostoon(tiedosto):
    
    pelin_tiedot["kesto"] = pelin_kesto[1] - pelin_kesto[0]

    pelien_tiedot = lataa_tiedot(TILASTOT_TIEDOSTONIMI)
    pelien_tiedot.append(pelin_tiedot)
    try:
        with open(tiedosto, "w") as kohde:
            json.dump(pelien_tiedot, kohde)
    except IOError:
        print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui.")




def lataa_tiedot(tiedosto):
    try:
        with open(tiedosto) as lahde:
            pelien_tiedot = json.load(lahde)
            return pelien_tiedot
    except (IOError, json.JSONDecodeError):
        print("Tiedoston avaaminen ei onnistunut. Aloitetaan tyhjällä kokoelmalla")
        pelien_tiedot = []
        try:
            with open(tiedosto, "w") as kohde:
                json.dump(pelien_tiedot, kohde)
        except IOError:
            print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui.")



def tulosta_tiedot(tiedosto):
    '''
    Tulostaa tilastotietoja pelatuista peleistä.
    '''
    
    pelien_tiedot = lataa_tiedot(TILASTOT_TIEDOSTONIMI)
    if pelien_tiedot == []:
        print()
        print("Sinulla ei ole vielä tilastoja, pelaa ensin peliä!")
        print()
    else:
        for i, peli in enumerate(pelien_tiedot):
            print()
            print("Peli {}:".format(i+1))
            print("Alkuaika: {}".format(peli["alkuaika"]))
            print("Loppuaika: {}".format(peli["loppuaika"]))
            print("Kesto: {:.2f} sekuntia".format(peli["kesto"]))
            print("Lopputulos: {}".format(peli["lopputulos"]))
            print("Miinojen lukumäärä: {}".format(peli["miinat"]))
            print("Kentän koko: {}".format(peli["koko"]))


def tyhjenna_tiedot(tiedosto):
    try:
        with open(tiedosto, "w") as kohde:
            pelien_tiedot = []
            json.dump(pelien_tiedot, kohde)
            print("Tilastot poistettu.")
    except (IOError, json.JSONDecodeError):
        print("Tiedoston avaaminen ei onnistunut.")


def kysy_dimensiot():
    '''
    Kysyy käyttäjältä miinakentän dimensiot
    '''
    
    while True:
        try:
            dimensiot["leveys"] = int(input("Anna kentän leveys: "))
            dimensiot["korkeus"] = int(input("Anna kentän korkeus: "))
            dimensiot["miina_lkm"] = int(input("Anna miinojen lukumäärä: "))
            pelin_tiedot["miinat"] = dimensiot["miina_lkm"]
            pelin_tiedot["koko"] = "{}x{}".format(dimensiot["leveys"], dimensiot["korkeus"])
            if dimensiot["leveys"] <= 0 or dimensiot["korkeus"] <= 0 or dimensiot["miina_lkm"] < 0:
                print("Anna positiivisia kokonaislukuja!")
                continue
            break
        except ValueError:
            print("Anna positiivinen kokonaisluku")
        else:
            break




def piirra_kentta():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    
    hr.tyhjaa_ikkuna()
    hr.piirra_tausta()
    hr.aloita_ruutujen_piirto()
    
    for y, rivi in enumerate(tila["kentta"]):
        for x, ruutu in enumerate(rivi):
            sarake = x * 40
            rivi = y * 40
            if tila["kentta"][y][x] == "x":
                hr.lisaa_piirrettava_ruutu(" ", sarake, rivi)
            elif tila["kentta"][y][x] == "X":
                hr.lisaa_piirrettava_ruutu("x", sarake, rivi)
            else:
                hr.lisaa_piirrettava_ruutu(tila["kentta"][y][x], sarake, rivi)

    hr.piirra_ruudut()




def tulvataytto(aloitus_x, aloitus_y):
    """
    Aloittaa täytön x, y pisteestä ja täyttää miinakentällä aloituspisteeseen 
    yhteydessä olevat miinattomat ruudut.
    """
    
    rivit_lkm = dimensiot["korkeus"]
    sarakkeet_lkm = dimensiot["leveys"]
    
    lista = [(aloitus_y, aloitus_x)]

    while len(lista) > 0:
        y, x = lista.pop(-1)
        tila["kentta"][y][x] = "0"
        for y1, rivi in enumerate(tila["kentta"]):
            if (y1 == y or y1 == y - 1 or y1 == y + 1) and rivit_lkm >= y1 >= 0:
                for x1, ruutu in enumerate(rivi):
                    if (x1 == x or x1 == x - 1 or x1 == x + 1) and sarakkeet_lkm >= x1 >= 0:
                        if ruutu == " " and laske_miinat(x1, y1) == 0:
                            lista.append((y1, x1))
                        elif laske_miinat(x1, y1) >= 1:
                            tila["kentta"][y1][x1] = str(laske_miinat(x1, y1))




def miinoita(kentta, vapaat_ruudut, N):
    """
    Asettaa kentälle N kpl miinoja satunnaisiin paikkoihin.
    """
    
    miinat = []
    
    miinat.extend(random.sample(vapaat_ruudut, N))
    
    for ruutu in miinat:
        rivi, sarake = ruutu
        tila["kentta"][sarake][rivi] = "x"




def luo_kentta(leveys, korkeus, miinat_lkm):
    '''
    Luo pelaajan toivomuksien kokoisen miinakentän peliä varten
    '''
    
    tila["kentta"] = []
    
    for rivi in range(korkeus):
        tila["kentta"].append([])
        for sarake in range(leveys):
            tila["kentta"][-1].append(" ")
            
    jaljella = []
    for x in range(leveys):
        for y in range(korkeus):
            jaljella.append((x, y))

    miinoita(tila["kentta"], jaljella, dimensiot["miina_lkm"])




def main():
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """

    hr.lataa_kuvat(GRAFIIKAT_POLKU)
    hr.luo_ikkuna(dimensiot["leveys"] * 40, dimensiot["korkeus"] * 40)
    hr.aseta_hiiri_kasittelija(kasittele_hiiri)
    hr.aseta_piirto_kasittelija(piirra_kentta)
    hr.aloita()




def kasittele_hiiri(x, y, painike, mnappaimet):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä.
    Palauttaa hiiren klikkauksen sijainnin koordinaatteina kentällä.
    """
    hiiripainikkeet = {
                    hr.HIIRI_VASEN: "vasen",
                    hr.HIIRI_OIKEA: "oikea",
                    hr.HIIRI_KESKI: "keski"
                    }
                    
    x = int(x / 40)
    y = int(y / 40)
    
    if hiiripainikkeet[painike] == "vasen":
        if tila["kentta"][y][x] == "x":
            print()
            print("Valitsemasi ruutu sisälsi miinan")
            print("Hävisit pelin!")
            print()
            paljasta_miinat()
            pelin_tiedot["loppuaika"] = time.strftime("%H:%M:%S %d-%m-%Y", time.localtime())
            pelin_tiedot["lopputulos"] = 'häviö'
            pelin_kesto[1] = timer()
            tallenna_tiedostoon(TILASTOT_TIEDOSTONIMI)
            tila["running"] = False
        elif tila["kentta"][y][x] != " ":
            print("Valitse ruutu jota ei ole vielä tutkittu")
        elif laske_miinat(x, y) != 0:
            tila["kentta"][y][x] = str(laske_miinat(x, y))
        elif laske_miinat(x, y) == 0:
            tulvataytto(x, y)
            
    jaljella_olevia()




def jaljella_olevia():
    '''
    Laskee, onko kentällä jäljellä avaamattomia ruutuja voittamista varten.
    '''
    
    tutkitut_rivit = 0
    
    for i, rivi in enumerate(tila["kentta"]):
        if rivi.count(" ") == 0:
            tutkitut_rivit += 1
    if tutkitut_rivit == i+1:
        print()
        print("Onneksi olkoon, voitit pelin!")
        print()
        paljasta_miinat()
        pelin_tiedot["loppuaika"] = time.strftime("%H:%M:%S %d-%m-%Y", time.localtime())
        pelin_tiedot["lopputulos"] = 'voitto'
        pelin_kesto[1] = timer()
        tallenna_tiedostoon(TILASTOT_TIEDOSTONIMI)
        tila["running"] = False




def laske_miinat(x, y):
    """
    Laskee ruudun ympärillä olevien miinojen lukumäärän
    """
    
    rivi_lkm = dimensiot["korkeus"]
    sarake_lkm = dimensiot["leveys"]
    miina_lkm = 0
    
    for y1, rivi in enumerate(tila["kentta"]):
        if (y1 == y or y1 == y + 1 or y1 == y - 1) and rivi_lkm >= y1 >= 0:
            for x1, ruutu in enumerate(rivi):
                if (x1 == x or x1 == x - 1 or x1 == x + 1) and sarake_lkm >= x1 >= 0:
                    if ruutu == 'x':
                        miina_lkm += 1
    return miina_lkm
    



def paljasta_miinat():
    '''
    Paljastaa kaikki kentällä olleet miinat pelin päättyessä.
    '''
    for y, rivi in enumerate(tila["kentta"]):
        for x, ruutu in enumerate(rivi):
            if ruutu == "x":
                tila["kentta"][y][x] = "X"




if __name__ == "__main__":
    print()
    print("Tervetuloa Miinantallaajaan!")
    print()
    print("Sinulla on neljä valintaa:")
    print("(A)loita uusi peli")
    print("(L)opeta")
    print("(K)atso tilastot")
    print("(P)oista tilastot")
    
    while True:
            valinta = input("Tee valintasi: ")
            if valinta == "a":
                tila["running"] = True
                pelin_tiedot["alkuaika"] = time.strftime("%H:%M:%S %d-%m-%Y", time.localtime())
                pelin_kesto[0] = timer()
                while tila["running"]:
                    kysy_dimensiot()
                    luo_kentta(dimensiot["leveys"], dimensiot["korkeus"], dimensiot["miina_lkm"])
                    main()
                    tila["running"] = False
            elif valinta == "l":
                print()
                print("Kiitos pelaamisesta")
                print("Näkemisiin!")
                break
            elif valinta == "k":
                tulosta_tiedot(TILASTOT_TIEDOSTONIMI)
            elif valinta == "p":
                tyhjenna_tiedot(TILASTOT_TIEDOSTONIMI)
            else:
                print()
                print("Valitsemaasi toimintoa ei ole olemassa")