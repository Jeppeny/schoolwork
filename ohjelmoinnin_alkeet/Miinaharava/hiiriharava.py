import haravasto as hr


def kasittele_hiiri(x, y, painike, mnappaimet):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä.
    Tulostaa hiiren sijainnin sekä painetun napin terminaaliin.
    """
    hiiripainikkeet = {
                    hr.HIIRI_VASEN: "vasen",
                    hr.HIIRI_OIKEA: "oikea",
                    hr.HIIRI_KESKI: "keski"
                    }
    print("Hiiren nappia {} painettiin kohdassa {}, {}".format(hiiripainikkeet[painike], x, y))


def main():
    """
    Luo sovellusikkunan ja asettaa käsittelijäfunktion hiiren klikkauksille.
    Käynnistää sovelluksen.
    """
    hr.luo_ikkuna(400, 400)
    hr.aseta_hiiri_kasittelija(kasittele_hiiri)
    hr.aloita()
    
    
if __name__ == "__main__":
    main()