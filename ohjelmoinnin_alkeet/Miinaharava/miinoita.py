import random
import haravasto as h


tila = {
    "kentta": []
}

def miinoita(kentta, vapaat_ruudut, N):
    """
    Asettaa kentälle N kpl miinoja satunnaisiin paikkoihin.
    """
    
    miinat = []
    
    miinat.extend(random.sample(vapaat_ruudut, N))
    
    for ruutu in miinat:
        rivi, sarake = ruutu
        kentta[sarake][rivi] = 'x'

def piirra_kentta():
    
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    
    h.tyhjaa_ikkuna()
    h.piirra_tausta()
    h.aloita_ruutujen_piirto()
    
    for y, rivi in enumerate(tila['kentta']):
        for x, ruutu in enumerate(rivi):
            sarake = x * 40
            rivi = y * 40
            if ruutu == 'x':
                h.lisaa_piirrettava_ruutu("x", sarake, rivi)
            else:
                h.lisaa_piirrettava_ruutu(" ", sarake, rivi)

    h.piirra_ruudut()
    
def main():
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """

    h.lataa_kuvat('/Users/Jesper/Documents/Programming/Miinaharava/spritet')
    h.luo_ikkuna(600, 400)
    h.aseta_piirto_kasittelija(piirra_kentta)
    h.aloita()
    
    
    
if __name__ == "__main__":
    kentta = []
    for rivi in range(10):
        kentta.append([])
        for sarake in range(15):
            kentta[-1].append(" ")

    tila["kentta"] = kentta


    jaljella = []
    for x in range(15):
        for y in range(10):
            jaljella.append((x, y))


    miinat_lkm = 35

    miinoita(kentta, jaljella, miinat_lkm)
    
    main()
