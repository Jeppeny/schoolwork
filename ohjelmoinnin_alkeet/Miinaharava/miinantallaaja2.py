import random
import haravasto as hr


tila = {"kentta" : [],
        "running" : True}


def kasittele_hiiri(x, y, painike, mnappaimet):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä.
    Palauttaa hiiren klikkauksen sijainnin koordinaatteina kentällä.
    """
    hiiripainikkeet = {
                    hr.HIIRI_VASEN: "vasen",
                    hr.HIIRI_OIKEA: "oikea",
                    hr.HIIRI_KESKI: "keski"
                    }
    
    x1 = int(x / 40)
    y1 = int(y / 40)
    print(x1)
    print(y1)
    if hiiripainikkeet[painike] == "vasen":
        if tila["kentta"][y1][x1] == "x":
            print("Valitsemasi ruutu sisälsi miinan")
            print("Hävisit pelin!")
            tila["running"] = False
        elif tila["kentta"][y1][x1] != " ":
            print("Valitse ruutu jota ei ole vielä tutkittu")
        else:
            if laske_miinat(x1, y1) == 0:
                tulvataytto(tila["kentta"], x1, y1)
                
            else:
                tila["kentta"][y1][x1] = "nr"
                
    for rivi in tila["kentta"]:
        print(rivi)


def piirra_kentta():
    
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    
    hr.tyhjaa_ikkuna()
    hr.piirra_tausta()
    hr.aloita_ruutujen_piirto()
    
    for y, rivi in enumerate(tila["kentta"]):
        for x, ruutu in enumerate(rivi):
            sarake = x * 40
            rivi = y * 40
            if ruutu == "nr":
                hr.lisaa_piirrettava_ruutu(str(laske_miinat(x, y)), sarake, rivi)
            elif ruutu == "0":
                hr.lisaa_piirrettava_ruutu("0", sarake, rivi)
            elif ruutu == "1" or ruutu == "2" or ruutu == "3" or ruutu == "4" or ruutu == "5" or ruutu == "6" or ruutu == "7" or ruutu == "8":
                hr.lisaa_piirrettava_ruutu(str(laske_miinat(x, y)), sarake, rivi)
            else:
                hr.lisaa_piirrettava_ruutu(" ", sarake, rivi)
            
            
            '''
            if ruutu == 'x':
                hr.lisaa_piirrettava_ruutu("x", sarake, rivi)
            elif ruutu == '0':
                hr.lisaa_piirrettava_ruutu("0", sarake, rivi)
            else:
                hr.lisaa_piirrettava_ruutu(" ", sarake, rivi)
            '''

    hr.piirra_ruudut()




def tulvataytto(kentta, aloitus_x, aloitus_y):
    """
    Aloittaa täytön x, y pisteestä ja täyttää miinakentällä aloituspisteeseen 
    yhteydessä olevat miinattomat ruudut.
    """
    
    rivit_lkm = len(tila["kentta"])
    sarakkeet_lkm = len(tila["kentta"][0])
    
    lista = [(aloitus_y, aloitus_x)]

    while len(lista) > 0:
        y, x = lista.pop(-1)
        tila["kentta"][y][x] = "0"
        for y1, rivi in enumerate(tila["kentta"]):
            if y1 == y or y1 == y - 1 or y1 == y + 1:
                for x1, ruutu in enumerate(rivi):
                    if x1 == x or x1 == x - 1 or x1 == x + 1:
                        if ruutu == " " and sarakkeet_lkm >= y1 >= 0 and rivit_lkm >= x1 >= 0 and laske_miinat(x1, y1) == 0:
                            lista.append((y1, x1))
                        elif ruutu == " " and sarakkeet_lkm >= y1 >= 0 and rivit_lkm >= x1 >= 0:
                            tila["kentta"][y1][x1] = str(laske_miinat(x1, y1))




def miinoita(kentta, vapaat_ruudut, N):
    """
    Asettaa kentälle N kpl miinoja satunnaisiin paikkoihin.
    """
    
    miinat = []
    
    miinat.extend(random.sample(vapaat_ruudut, N))
    
    for ruutu in miinat:
        rivi, sarake = ruutu
        tila["kentta"][sarake][rivi] = "x"




def laske_miinat(x, y):

    """
    Laskee ruudun ympärillä olevien miinojen lukumäärän
    """
    
    rivi_lkm = len(tila["kentta"])
    sarake_lkm = len(tila["kentta"][0])
    miina_lkm = 0
    
    for y1, rivi in enumerate(tila["kentta"]):
        if y1 == y or y1 == y + 1 or y1 == y - 1:
            for x1, ruutu in enumerate(rivi):
                if x1 == x or x1 == x - 1 or x1 == x + 1:
                    if ruutu == 'x' and sarake_lkm >= y1 >= 0 and rivi_lkm >= x1 >= 0:
                        miina_lkm += 1
                        
    return miina_lkm
    
    
def luo_kentta(leveys, korkeus, miinat_lkm):
    '''
    Luo pelaajan toivomuksien kokoisen miinakentän peliä varten
    '''
    
    
    for rivi in range(korkeus):
        tila["kentta"].append([])
        for sarake in range(leveys):
            tila["kentta"][-1].append(" ")
            
    jaljella = []
    for x in range(leveys):
        for y in range(korkeus):
            jaljella.append((x, y))

    miinoita(tila["kentta"], jaljella, miinat_lkm)




def main():
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """

    hr.lataa_kuvat('/Users/Jesper/Documents/Programming/Miinaharava/spritet')
    hr.luo_ikkuna(50 * 40, 10 * 40)
    hr.aseta_hiiri_kasittelija(kasittele_hiiri)
    hr.aseta_piirto_kasittelija(piirra_kentta)
    hr.aloita()
    



if __name__ == "__main__":
    luo_kentta(50, 30, 10)
    main()
    