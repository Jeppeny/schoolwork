def laske_sivun_pituus(hypotenuusa):
    katetin_pituus = (hypotenuusa ** 2 / 2) ** 0.5
    return katetin_pituus
    
hyp_pituus = float(input("Anna tasakylkisen kolmion hypotenuusan pituus: "))
kat_pituus = laske_sivun_pituus(hyp_pituus)
print("Kylkien pituus: ", round(kat_pituus, 4))