def summa():
    try:
        luku_1 = float(input("Anna luku 1: "))
        luku_2 = float(input("Anna luku 2: "))
    except ValueError:
        print("Ei tämä ole mikään luku")
    else:
        summa = luku_1 + luku_2
        print("Tulos: {:.1f}".format(summa))
    
def erotus():
    try:
        luku_1 = float(input("Anna luku 1: "))
        luku_2 = float(input("Anna luku 2: "))
    except ValueError:
        print("Ei tämä ole mikään luku")
    else:
        erotus = luku_1 - luku_2
        print("Tulos: {:.1f}".format(erotus))

def tulo():
    try:
        luku_1 = float(input("Anna luku 1: "))
        luku_2 = float(input("Anna luku 2: "))
    except ValueError:
        print("Ei tämä ole mikään luku")
    else:
        tulo = luku_1 * luku_2
        print("Tulos: {:.1f}".format(tulo))

def osamaara():
    try:
        luku_1 = float(input("Anna luku 1: "))
        luku_2 = float(input("Anna luku 2: "))
    except ValueError:
        print("Ei tämä ole mikään luku")
    else:
        if luku_2 == 0:
            print("Tällä ohjelmalla ei pääse äärettömyyteen")
        else:
            osamaara = luku_1 / luku_2
            print("Tulos: {:.1f}".format(osamaara))

operaatio = input("Valiste operaatio (+, -, *, /): ")

if operaatio == "+":
    summa()
elif operaatio == "-":
    erotus()
elif operaatio == "*":
    tulo()
elif operaatio == "/":
    osamaara()
else:
    print("Operaatiota ei ole olemassa")
