def pyyda_syote(pyynto, virheviesti):

    """
    Kysyy käyttäjältä kokonaislukua käyttäen kysymyksenä parametrina annettua
    merkkijonoa. Virheellisen syötteen kohdalla käyttäjälle näytetään toisena
    parametrina annettu virheilmoitus. Käyttäjän antama kelvollinen syöte
    palautetaan kokonaislukuna. Hyväksyy vain luvut jotka ovat suurempia kuin 1.
    """
    

    while True:
        try:
            luku = int(input(pyynto))
            if luku <= 1:
                print(virheviesti)
            else:
                return luku
        except ValueError:
            print(virheviesti)
            
            
            
            
def tarkista_alkuluku(luku):

    """
    Tarkistaa onko parametrina annettu luku alkuluku. Palauttaa False jos luku ei
    ole alkuluku; jos luku on alkuluku palautetaan True
    """
    
    for i in range(2, luku):
        f = True
        if (luku % i) == 0:
            f = False
            break

    return f


x = tarkista_alkuluku(pyyda_syote("Anna kokonaisluku joka on suurempi kuin 1", "sus amogus"))

if x:
    print("prime number")
else:
    print("not a prime number")
