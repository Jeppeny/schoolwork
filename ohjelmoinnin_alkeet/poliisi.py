def laske_nopeus(d, t):
    nopeus = (d/1000)/(t/3600)
    return nopeus

try:
    d = float(input("Anna auton kulkema matka (m): "))
    t = float(input("Anna matkaan kulunut aika (s): "))
except ValueError:
    print("Vähemmän donitseja, enemmän puhtaita numeroita.")
else:
    v = laske_nopeus(d, t)
    print("{:.2f} metriä {:.2f} sekunnissa kulkeneen auton nopeus on {:.2f} km/h".format(d, t, v))

