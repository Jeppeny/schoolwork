def pyyda_syote(pyynto, virheviesti):

    """
    Kysyy käyttäjältä kokonaislukua käyttäen kysymyksenä parametrina annettua
    merkkijonoa. Virheellisen syötteen kohdalla käyttäjälle näytetään toisena
    parametrina annettu virheilmoitus. Käyttäjän antama kelvollinen syöte
    palautetaan kokonaislukuna. Hyväksyy vain luvut jotka ovat suurempia kuin 1.
    """
    

    while True:
        try:
            luku = int(input(pyynto))
            if luku <= 1:
                print(virheviesti)
            else:
                return luku
        except ValueError:
            print(virheviesti)
