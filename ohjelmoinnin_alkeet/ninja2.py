def laske_ninjat(x, y, huone):

    """
    Laskee annetussa huoneessa yhden ruudun ympärillä olevat ninjat ja palauttaa
    niiden lukumäärän. Funktio toimii sillä oletuksella, että valitussa ruudussa ei
    ole ninjaa - jos on, sekin lasketaan mukaan.
    """
    
    rivi_lkm = len(huone)
    sarake_lkm = len(huone[0])
    ninja_lkm = 0
    
    for y1, rivi in enumerate(huone):
        if y1 == y or y1 == y + 1 or y1 == y - 1:
            for x1, ruutu in enumerate(rivi):
                if x1 == x or x1 == x - 1 or x1 == x + 1:
                    if ruutu == 'N' and sarake_lkm >= y1 >= 0 and rivi_lkm >= x1 >= 0:
                        ninja_lkm += 1
                        
    return ninja_lkm