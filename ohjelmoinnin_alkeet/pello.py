ELAIMET = {
    "a": "aasi",
    "k": "koira",
    "@": "kissa",
    "h": "hemuli",
    "l": "lammas"
}

def tutki_ruutu(merkki, rivinro, sarakenro):

    """
    Funktio tutkii ruudun - jos siellä on eläin, se tulostaa eläimen sijainnin sekä nimen.
    """
    
    for key in ELAIMET.keys():
        if key == merkki:
            print("Ruudusta {}, {} löytyy {}".format(sarakenro, rivinro, ELAIMET[merkki]))
        


def tutki_kentta(kentta):

    """
    Funktio tutkii kentän sisällön käymällä sen kokonaan läpi kutsuen tutki_ruutu-funktiota
    jokaiselle kentän sisällä olevalle alkiolle.
    """

    for y, rivi in enumerate(kentta):
        rivinro = y
        rivi = rivi
        for x, ruutu in enumerate(rivi):
            sarakenro = x
            merkki = ruutu
            tutki_ruutu(merkki, rivinro, sarakenro)