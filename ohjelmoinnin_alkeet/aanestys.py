verouudistus = {
    "jaa": 0,
    "ei": 0,
    "eos": 0,
    "virhe": 0
}
nalle_puh_presidentiksi = {
    "jaa": 12,
    "ei": 0,
    "eos": 5,
    "virhe": 4
}

def aanesta(sanakirja):
    print("Anna äänesi, vaihtoehdot ovat:")
    print("jaa, ei, eos")
    valinta = input(": ").lower()
    if valinta == "jaa":
        sanakirja["jaa"] += 1
    elif valinta == "ei":
        sanakirja["ei"] += 1
    elif valinta == "eos":
        sanakirja["eos"] += 1
    else:
        sanakirja["virhe"] += 1
    
def nayta_tulokset(sanakirja):
    print("Jaa: ", "#" * sanakirja["jaa"])
    print("Ei: ", "#" * sanakirja["ei"])
    print("Eos: ", "#" * sanakirja["eos"])
    print("Virhe: ", "#" * sanakirja["virhe"])
    
print("Suoritetaanko verouudistus?")
aanesta(verouudistus)
print()
nayta_tulokset(verouudistus)
print()
print("Nalle Puh presidentiksi?")
aanesta(nalle_puh_presidentiksi)
print()
nayta_tulokset(nalle_puh_presidentiksi)