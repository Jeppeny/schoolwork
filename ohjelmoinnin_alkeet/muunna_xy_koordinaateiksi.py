import math as m

def muunna_xy_koordinaateiksi(alpha, v_length):
    x = int(round(v_length * m.cos(alpha)))
    y = int(round(v_length * m.sin(alpha)))
    return x, y