from math import pi

def laske_sektorin_ala(r, alpha):
    sektorin_ala = alpha * pi * r ** 2 / 360
    return sektorin_ala

sade = float(input("Anna ympyrän säde: "))
sisakulma = float(input("Anna sektorin sisäkulma asteina: "))
sektorin_ala = laske_sektorin_ala(sade, sisakulma)
print("Sektorin pinta-ala: ", round(sektorin_ala, 4))