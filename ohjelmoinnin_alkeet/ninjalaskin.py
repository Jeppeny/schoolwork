def laske_ninjat(x, y, huone):
    
    rivi_lkm = len(huone)
    sarake_lkm = len(huone[0])
    ninja_lkm = 0
    
    for x1, y1 in [(0, 0), (1, 1), (0, 1), (1, 0), (-1, -1), (-1, 0), (0, -1), (1, -1), (-1, 1)]:
        yy = y + y1
        xx = x + x1
        
        try:
            if huone[yy][xx] == 'N' and rivi_lkm >= yy >= 0 and sarake_lkm >= xx >= 0:
                ninja_lkm += 1
        except IndexError:
            pass
                        
    return ninja_lkm
