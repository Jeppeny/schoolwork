from math import pi

def laske_nelion_ala(x):
    float(x)
    nelion_ala = x ** 2
    return nelion_ala

def laske_sektorin_ala(r, alpha):
    sektorin_ala = alpha * pi * r ** 2 / 360
    return sektorin_ala

def laske_sivun_pituus(hypotenuusa):
    katetin_pituus = (hypotenuusa ** 2 / 2) ** 0.5
    return katetin_pituus
    
def laske_kuvion_ala(x):
    suurempi_nelio_sivun_pituus = laske_sivun_pituus(x) * 2
    suuri_ympyrasektori_ala = laske_sektorin_ala(suurempi_nelio_sivun_pituus, 270)
    suurempi_nelio_ala = laske_nelion_ala(suurempi_nelio_sivun_pituus)
    pienempi_nelio_ala = laske_nelion_ala(x)
    pieni_kolmio_ala = laske_sivun_pituus(x) ** 2 / 2
    pieni_sektori_ala = laske_sektorin_ala(laske_sivun_pituus(x), 45)
    kuvion_ala = suuri_ympyrasektori_ala + suurempi_nelio_ala + pieni_kolmio_ala + pieni_sektori_ala + pienempi_nelio_ala
    return kuvion_ala
    
x = float(input("Anna x: "))
kuvion_ala = laske_kuvion_ala(x)
print("Kuvion ala: ", round(kuvion_ala, 4))