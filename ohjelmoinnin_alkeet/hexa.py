def muotoile_heksaluvuksi(luku, bitit):
    heksa = hex(luku).lstrip("0x").zfill(bitit // 4)
    return heksa
    
try:
    luku = int(input("Anna kokonaisluku: "))
    bitit = int(input("Anna heksaluvun pituus (bittien lukumäärä): "))
except ValueError:
    print("Kokonaisluku kiitos")
else:
    print(muotoile_heksaluvuksi(luku, bitit))