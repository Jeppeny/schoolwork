from turtle import *

def piirra_tiedostosta(tiedosto):
    with open(tiedosto) as lahde:
        for rivi in lahde.readlines():
            vari, kaari_lkm, alkusade, sateen_kasvu, viiva_paksuus = rivi.split(',')
            piirra_spiraali(vari.strip(), int(kaari_lkm.strip()), int(alkusade.strip()), float(sateen_kasvu.strip()), int(viiva_paksuus.strip()))

def piirra_spiraali(vari, kaari_lkm, alkusade, sateen_kasvu, viiva_paksuus=1):

    color(vari)
    pensize(viiva_paksuus)
    
    for i in range(kaari_lkm):
        circle(alkusade, 90)
        alkusade = alkusade + sateen_kasvu