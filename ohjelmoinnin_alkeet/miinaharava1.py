TULOSTUKSET = {
    "ulkona": "Antamasi ruutu on kentän ulkopuolella.",
    "nurkassa": "Antamasi ruutu on kentän nurkassa.",
    "laidalla": "Antamasi ruutu on kentän laidalla.",
    "keskellä": "Antamasi ruutu on keskikentällä."
}

def sijainti_kentalla(x, y, leveys, korkeus):
    if (x == 0 and y == 0) or (x == 0 and y == korkeus -1) or (x == leveys -1 and y == korkeus -1) or (y == 0 and x == leveys - 1):
        return "nurkassa"
    elif (x == 0 and 0 <= y <= korkeus - 1) or (y == korkeus - 1 and 0 <= x <= leveys -1) or (x == leveys -1 and 0 <= y <= korkeus -1) or (y == 0 and 0 <= x <= leveys -1):
        return "laidalla"
    elif 2 <= x <= leveys - 2 and 2 <= y <= korkeus -2:
        return "keskellä"
    else:
        return "ulkona"
        
def tulosta_sijainti(sijainti):
    print(TULOSTUKSET[sijainti])


##pääohjelma
try:
    leveys = int(input("Anna kentän leveys: "))
    korkeus = int(input("Anna kentän korkeus: "))
except ValueError:
    print("Ei näytä luvulta")
else:
    if leveys <= 0 or korkeus <= 0:
        print("Noin pienelle kentälle ei mahdu ainuttakaan ruutua!")
    else:
        x = int(input("Anna x-koordinaatti: "))
        y = int(input("Anna y-koordinaatti: "))
        tulosta_sijainti(sijainti_kentalla(x, y, leveys, korkeus))