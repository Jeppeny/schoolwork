def nayta_tulokset(tiedosto):
    with open(tiedosto) as kohde:
        for rivi in kohde.readlines():
            pelaaja1nimi, pelaaja2nimi, pelaaja1pisteet, pelaaja2pisteet = rivi.split(",")
            print("{} {} - {} {}".format(pelaaja1nimi.strip(), int(pelaaja1pisteet), int(pelaaja2pisteet), pelaaja2nimi.strip()))