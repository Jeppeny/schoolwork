import numpy as np

def NormalizeData(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

array = [
   [ 1.1,  2.1],
   [ 1.8,  0.7],
    [ 1.2,  2.2],
    [ 0.4,  0.7],
    [2.9, 3.3],
    [0.6, 0.3],
    [2.2, 1.1],
    [0.9, 1.9],
    [2.1, 0.8],
    [0.1, 0.2],
    [3.1, 3.4]
]

def FormArrays(array):
    X = []
    Y = []
    for item in array:
        X.append([item[0]])
        Y.append([item[1]])
    Xnp = np.array(X)
    Ynp = np.array(Y)
    scaled_x = NormalizeData(Xnp)
    scaled_y = NormalizeData(Ynp)
    print(scaled_x)
    print(scaled_y)
    
FormArrays(array)
  
'''  
X = np.array([
    [ 3.2],
    [ 1.1],
    [ 1.8],
    [ 1.2],
    [ 0.4],
    [2.9],
    [0.6],
    [2.2],
    [0.9],
    [2.1],
    [0.1],
    [3.1]
])

Y = np.array([
    [2.5],
    [2.1],
    [0.7],
    [2.2],
    [0.7],
    [3.3],
    [0.3],
    [1.1],
    [1.9],
    [0.8],
    [0.2],
    [3.4]
])

scaled_x = NormalizeData(X)
scaled_y = NormalizeData(Y)

print(scaled_x)
print(scaled_y)
'''