from turtle import *

def piirra_nelio(sivu, x, y):
    up()
    setx(x)
    sety(y)
    
    if x < 0:
        color("red")
        begin_fill()
        down()
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        end_fill()
        up()
        
    
    elif x > 0:
        color("blue")
        begin_fill()
        down()
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        forward(sivu)
        left(90)
        end_fill()
        up()
        
    setx(0)
    sety(0)

piirra_nelio(40, -100, 100)
piirra_nelio(60, 100, -100)
piirra_nelio(100, -50, -20)
piirra_nelio(80, 90, 30)
done()