import java.util.List;
import java.util.Scanner;

public class Vt6main {
	
	public static void main(String [] args) {
		String tiedosto = "sanat.txt";
		int arvaukset = 10;
		System.out.println("Anna sanat sisältävän tiedoston nimi / polku >");
		tiedosto = sc.nextLine();
		System.out.println("Anna sallittava arvausten määrä >");
		arvaukset = sc.nextInt();
		sc.nextLine();
		
		Sanalista sanalista = new Sanalista(tiedosto);
		List<String> sanat = sanalista.annaSanat();

		
		Hirsipuu hirsipuu = new Hirsipuu(sanat, arvaukset);
		
		while (!hirsipuu.onLoppu() && hirsipuu.arvauksiaOnJaljella() > 0) {
			System.out.println("Arvauksia jäljellä: " + hirsipuu.arvauksiaOnJaljella() + "\n");
			System.out.println("Syötä uusi merkki >");
			Character arvaus = sc.next().charAt(0);
			hirsipuu.arvaa(arvaus);
			if (hirsipuu.onOikein(arvaus)) System.out.print("Oikein! ");
			if (!hirsipuu.onOikein(arvaus)) System.out.print("Väärin! ");
			hirsipuu.naytaOikeinArvatut();
			System.out.println();
		}
		
		if (hirsipuu.onLoppu()) {
			System.out.println("Voitit! Sana oli: " + hirsipuu.sana());
		}
		else if (hirsipuu.arvauksiaOnJaljella() == 0) System.out.println("Hävisit! Sana oli: " + hirsipuu.sana());
		
	}
	
	private static final Scanner sc = new Scanner(System.in);
}
