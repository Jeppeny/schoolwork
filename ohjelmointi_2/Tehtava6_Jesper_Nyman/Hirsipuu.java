import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class Hirsipuu {
	
	private String arvattavaSana;
	private int arvaustenMaara;
	private List<Character> arvaukset = new ArrayList<>();
	
	public Hirsipuu(List<String> sanat, int arvausLkm) {
	    Random rand = new Random();
	    String sana = sanat.get(rand.nextInt(sanat.size()));
	    this.arvattavaSana = sana;
	    this.arvaustenMaara = arvausLkm;
	}
	
	public boolean arvaa(Character merkki) {
		arvaukset.add(merkki);
		if (arvattavaSana.indexOf(merkki) != -1) return true;
		arvaustenMaara -= 1;
		return false;
	}
	
	public List<Character> arvaukset() {
		return arvaukset;
	}
	
	public int arvauksiaOnJaljella() {
		return arvaustenMaara;
	}
	
	public String sana() {
		return arvattavaSana;
	}
	
	public boolean onOikein(Character merkki) {
		List<Character> arvattavaSanaMerkit = new ArrayList<>();
        for (int i = 0; i < arvattavaSana.length(); i++) {
            arvattavaSanaMerkit.add(arvattavaSana.charAt(i));
        }
        if (arvattavaSanaMerkit.contains(merkki)) return true;
        return false;
	}
	
	public void naytaOikeinArvatut() {
		String oikeinArvatut = "";
		List<Character> arvattavaSanaMerkit = new ArrayList<>();
        for (int i = 0; i < arvattavaSana.length(); i++) {
            arvattavaSanaMerkit.add(arvattavaSana.charAt(i));
        }
        for (int i = 0; i < arvattavaSanaMerkit.size(); i++) {
            if (arvaukset.contains(arvattavaSanaMerkit.get(i))) oikeinArvatut += arvattavaSanaMerkit.get(i);
            if (!arvaukset.contains(arvattavaSanaMerkit.get(i))) oikeinArvatut += "_";
        }
        
        System.out.println("Sana: " + oikeinArvatut);
	}
	
	public boolean onLoppu() {
		char [] merkit = arvattavaSana.toCharArray();
		for (Character merkki : merkit) {
			if (arvaukset.indexOf(merkki) == -1) {
				return false;
			}
		}
		return true;
	}

	public void setArvattavaSana(String arvattavaSana) {
		this.arvattavaSana = arvattavaSana;
	}

	public void setArvaustenMaara(int arvaustenMaara) {
		this.arvaustenMaara = arvaustenMaara;
	}

	public void setArvaukset(List<Character> arvaukset) {
		this.arvaukset = arvaukset;
	}
	
}
