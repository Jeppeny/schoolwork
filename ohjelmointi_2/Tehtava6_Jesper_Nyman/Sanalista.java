import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sanalista {
	
	private List<String> sanat = new ArrayList<>();
	
	public Sanalista sanatJoidenPituusOn(int pituus, String tiedostoNimi) {
		Sanalista sanalista1 = new Sanalista(tiedostoNimi);
		List<String> sanat1 = new ArrayList<>();
		for (String sana : sanat) {
			if (sana.length() == pituus) sanat1.add(sana);
		}
		sanalista1.setSanat(sanat1);
		return sanalista1;
	}
	
	public Sanalista sanatJoissaMerkit(String mjono, String tiedostoNimi) {
		Sanalista sanalista1 = new Sanalista(tiedostoNimi);
		List<String> oikein = new ArrayList<>();
		List<String> vaarin = new ArrayList<>();
		
		List<Character> merkit = new ArrayList<>();
        for (int i = 0; i < mjono.length(); i++) {
            merkit.add(mjono.charAt(i));
        }
		
		for (String sana : sanat) {
			
			List<Character> sanaMerkit = new ArrayList<>();
	        for (int i = 0; i < sana.length(); i++) {
	            sanaMerkit.add(sana.charAt(i));
	        }
			
			for (int i=0;i<merkit.size();i++) {
				if (merkit.get(i) != '_' && i<sanaMerkit.size()) {
					if (sanaMerkit.get(i) != merkit.get(i)) {
						vaarin.add(sana);
					}
				}
			}
			if (!vaarin.contains(sana)) {
				oikein.add(sana);
			}
			
		}
		sanalista1.setSanat(oikein);
		return sanalista1;
	}
	
	public Sanalista(String tiedostoNimi) {
		try (BufferedReader teksti = new BufferedReader(new FileReader(tiedostoNimi))) {
		  	String rivi;
		    while ((rivi = teksti.readLine()) != null) {
		        this.sanat.add(rivi);
		    } 
		} catch (FileNotFoundException err) {
			err.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setSanat(List<String> sanat) {
		this.sanat = sanat;
	}

	public List<String> annaSanat() {
		return sanat;
	}	
	
}
