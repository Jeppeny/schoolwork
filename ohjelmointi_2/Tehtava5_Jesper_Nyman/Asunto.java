import java.util.ArrayList;
import java.util.List;


public class Asunto {
	private double asuntoAla;
	private int huoneLkm;
	private List<Asukas> asukkaat = new ArrayList<>();
	
	
	public Asunto(double ala, int huoneet) {
		this.asuntoAla = ala;
		this.huoneLkm = huoneet;
		this.asukkaat = new ArrayList<>();
	}
	
	public void lisaaTiedot(Asukas tiedot) {
		asukkaat.add(tiedot);
	}

	public double getAsuntoAla() {
		return asuntoAla;
	}

	public void setAsuntoAla(double asuntoAla) {
		this.asuntoAla = asuntoAla;
	}

	public int getHuoneLkm() {
		return huoneLkm;
	}

	public void setHuoneLkm(int huoneLkm) {
		this.huoneLkm = huoneLkm;
	}

	@Override
	public String toString() {
		return "Asunto [asuntoAla=" + asuntoAla + ", huoneLkm=" + huoneLkm + ", asukkaat=" + asukkaat + "]";
	}
	
	public void tulostaAsunnonTiedot() {
		System.out.println("Asunnon pinta-ala on: " + asuntoAla);
		System.out.println("Asunnossa on " + huoneLkm + " huonetta");
		System.out.println();
		for(int i = 0; i < asukkaat.size(); i++) {
			Asukas a = asukkaat.get(i);
			System.out.println("Asukas " + (i+1) + ":");
			a.tulosta();
			}
	}
}
