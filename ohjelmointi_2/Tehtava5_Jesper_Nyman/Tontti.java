import java.util.ArrayList;
import java.util.List;

public class Tontti {
	//Tonttia mallintava luokka
	
	private String osoite;
	private String nimi;
	private double ala;
	private List<Rakennus> rakennukset = new ArrayList<>();
	
	public void lisaaTiedot(Rakennus tiedot) {
		rakennukset.add(tiedot);
	}
	
	public Tontti() {
		this(null, null, 0);
	}
	public Tontti(String osoite, String nimi, double ala) {
		this.osoite = osoite;
		this.nimi = nimi;
		this.ala = ala;
		this.rakennukset = new ArrayList<>();
	}
	public String getOsoite() {
		return osoite;
	}
	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public Double getAla() {
		return ala;
	}
	public void setAla(Double ala) {
		this.ala = ala;
	}

	public void tulostus() {
		System.out.println("Tontin osoite on " + osoite);
		System.out.println();
		System.out.println("Tontin nimi on " + nimi);
		System.out.println();
		System.out.println("Tontin pinta-ala on " + ala + " neliömetriä");
		System.out.println();
		Rakennus r = rakennukset.get(0);
		System.out.println("Rakennus: ");
		System.out.println("Rakennuksen tyyppi on " + r.getClass());
		r.tulostus();
		System.out.println();
	}
	
}
