import java.util.Scanner;

public class Vt5main {
	
	public static void main(String [] args) {
		
		System.out.println("Anna tontin osoite > ");
		String osoite = sc.nextLine();
		System.out.println("Anna tontin nimi > ");
		String nimi = sc.nextLine();
		
		
		System.out.println("Anna tontin pinta-ala > ");
		
		while(true) {
			double ala = sc.nextDouble();
			if(ala < 0) {
				System.out.println("Pinta-ala ei saa olla negatiivinen! Yritä uudestaan > ");
			}
			else {
		
				Tontti t1 = new Tontti(osoite, nimi, ala);
				
				System.out.println("Valitse tontilla sijaitsevan rakennuksen tyyppi > ");
				System.out.println("Omakotitalo = 1, Rivitalo = 2, Kerrostalo = 3 > ");
				int tyyppi = sc.nextInt();
				sc.nextLine();
		
		
				if(tyyppi == 1) {
					Rakennus r1 = new Omakotitalo();
					System.out.println("Anna rakennuksessa olevien asuntojen lukumäärä > ");
					int asunnot = sc.nextInt();
					r1.setAsuntoLkm(asunnot);
					for(int i = 0; i < asunnot; i++) {
						System.out.println("Anna asunnon " + (i+1) + " tiedot > ");
						System.out.println("Anna asunnon pinta-ala > ");
						double asuntoAla = sc.nextDouble();
						sc.nextLine();
						System.out.println("Anna asunnon huoneiden lukumäärä > ");
						int huoneLkm = sc.nextInt();
						Asunto asunto = new Asunto(asuntoAla, huoneLkm);
						System.out.println("Montako asukasta asunnossa asuu > ");
						int asukasLkm = sc.nextInt();
						sc.nextLine();
						for(int a = 0; a < asukasLkm; a++) {
							System.out.println("Anna asukkaan " + (a+1) + " nimi > ");
							String asukasNimi = sc.nextLine();
							Asukas asukas = new Asukas(asukasNimi);
							asunto.lisaaTiedot(asukas);
						}
						r1.lisaaTiedot(asunto);
					t1.lisaaTiedot(r1);
						}
				}
				else if(tyyppi == 2) {
					Rakennus r1 = new Rivitalo();
					System.out.println("Anna rakennuksessa olevien asuntojen lukumäärä > ");
					int asunnot = sc.nextInt();
					r1.setAsuntoLkm(asunnot);
					for(int i = 0; i < asunnot; i++) {
						System.out.println("Anna asunnon " + (i+1) + " tiedot > ");
						System.out.println("Anna asunnon pinta-ala > ");
						double asuntoAla = sc.nextDouble();
						sc.nextLine();
						System.out.println("Anna asunnon huoneiden lukumäärä > ");
						int huoneLkm = sc.nextInt();
						Asunto asunto = new Asunto(asuntoAla, huoneLkm);
						System.out.println("Montako asukasta asunnossa asuu > ");
						int asukasLkm = sc.nextInt();
						sc.nextLine();
						for(int a = 0; a < asukasLkm; a++) {
							System.out.println("Anna asukkaan " + (a+1) + " nimi > ");
							String asukasNimi = sc.nextLine();
							Asukas asukas = new Asukas(asukasNimi);
							asunto.lisaaTiedot(asukas);
						}
						r1.lisaaTiedot(asunto);
					t1.lisaaTiedot(r1);
						}
				}
				else if(tyyppi == 3) {
					Rakennus r1 = new Kerrostalo();
					System.out.println("Anna rakennuksessa olevien asuntojen lukumäärä > ");
					int asunnot = sc.nextInt();
					r1.setAsuntoLkm(asunnot);
					for(int i = 0; i < asunnot; i++) {
						System.out.println("Anna asunnon " + (i+1) + " tiedot > ");
						System.out.println("Anna asunnon pinta-ala > ");
						double asuntoAla = sc.nextDouble();
						sc.nextLine();
						System.out.println("Anna asunnon huoneiden lukumäärä > ");
						int huoneLkm = sc.nextInt();
						Asunto asunto = new Asunto(asuntoAla, huoneLkm);
						System.out.println("Montako asukasta asunnossa asuu > ");
						int asukasLkm = sc.nextInt();
						sc.nextLine();
						for(int a = 0; a < asukasLkm; a++) {
							System.out.println("Anna asukkaan " + (a+1) + " nimi > ");
							String asukasNimi = sc.nextLine();
							Asukas asukas = new Asukas(asukasNimi);
							asunto.lisaaTiedot(asukas);
						}
						r1.lisaaTiedot(asunto);
					t1.lisaaTiedot(r1);
						}
				}
				t1.tulostus();
			}
		}


	}
	
	public final static Scanner sc = new Scanner(System.in);
	//lukijalla saamme ohjelmaan syötteen käyttäjältä
}
