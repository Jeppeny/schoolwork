
public class Asukas {
	
	private String nimi;
	
	public Asukas() {
		this(null);
	}
	public Asukas(String nimi) {
		this.nimi = nimi;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}



	public void tulostus() {
		System.out.println("Asukkaan nimi on " + nimi);
	}
	
	@Override
	public String toString() {
		return "Asukas [nimi=" + nimi + "]";
	}
	
	public void tulosta() {
		System.out.println("Nimi: " + nimi);
	}
}
