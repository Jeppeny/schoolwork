import java.util.ArrayList;
import java.util.List;

public class Rakennus {
	//Tontilla sijaitsevaa rakennusta mallintava luokka
	
	private int asuntoLkm;
	private List<Asunto> asunnot = new ArrayList<>();
	
	public void lisaaTiedot(Asunto tiedot) {
		asunnot.add(tiedot);
	}
	
	public Rakennus() {
		this(0);
	}
	public Rakennus(int asuntoLkm) {
		this.asuntoLkm = asuntoLkm;
	}
	public int getAsuntoLkm() {
		return asuntoLkm;
	}
	public void setAsuntoLkm(int asuntoLkm) {
		this.asuntoLkm = asuntoLkm;
	}
	
	public void tulostus() {
		System.out.println("Rakennuksen asuntojen lukumäärä on " + asuntoLkm);
		System.out.println();
		for(int i = 0; i < asunnot.size(); i++) {
			Asunto a = asunnot.get(i);
			System.out.println("Asunto " + (i+1) + ":");
			a.tulostaAsunnonTiedot();
			System.out.println();
			System.out.println();
			}
	}

	@Override
	public String toString() {
		return "Rakennus [asuntoLkm=" + asuntoLkm + ", asunnot=" + asunnot + "]";
	}	
	
	
}
