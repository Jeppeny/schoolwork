
public class PankkitiliMain {

	public static void main(String [] args) {
	
		Pankkitili pankkitili = new Pankkitili();
		//luodaan uusi pankkitili
		
		
		//kysytään seuraavaksi nimi, tilinumero ja saldo:
		
		System.out.print("Anna nimesi> ");
		String uusiNimi = pankkitili.lukija.nextLine();
		pankkitili.setNimi(uusiNimi);
		
		System.out.print("Anna tilinumerosi> ");
		String uusiTiliNro = pankkitili.lukija.nextLine();
		pankkitili.setPankkiTiliNro(uusiTiliNro);
		
		System.out.print("Anna saldosi> ");
		Double uusiSaldo = pankkitili.lukija.nextDouble();
		pankkitili.setSaldo(uusiSaldo);
		
		pankkitili.tulostus();
		//tulostetaan tilin tämänhetkeiset tiedot
		
		System.out.print("Anna tililtä nostettava määrä> ");
		Double nostoMaara = pankkitili.lukija.nextDouble();
		pankkitili.otto(nostoMaara);
		//suoritetaan nosto
		
		System.out.print("Anna tilille talletettava määrä> ");
		Double talletusMaara = pankkitili.lukija.nextDouble();
		pankkitili.talletus(talletusMaara);
		//suoritetaan talletus
		
		pankkitili.tulostus();
		//tulostetaan tilin tämänhetkeiset tiedot

	}
}
