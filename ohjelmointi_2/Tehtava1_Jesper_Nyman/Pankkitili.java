import java.util.Scanner;


public class Pankkitili {
//tehdään uusi luokka Pankkitili ja asetetaan vaaditut jäsenmuuttujat
	private String nimi;
	private String pankkiTiliNro;
	private double saldo;
	
//seuraavaksi toteutetaan jäsenmuuttujien asetus- sekä saantimetodit:
	public String getNimi() {
		return nimi;
	}



	public void setNimi(String nimi) {
		this.nimi = nimi;
	}



	public String getPankkiTiliNro() {
		return pankkiTiliNro;
	}



	public void setPankkiTiliNro(String pankkiTiliNro) {
		this.pankkiTiliNro = pankkiTiliNro;
	}



	public double getSaldo() {
		return saldo;
	}



	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}



	public final Scanner lukija = new Scanner(System.in);
	//lukijalla saamme ohjelmaan syötteen käyttäjältä
	
	public void otto(double ottoMaara) {
		//ottometodilla voidaan toteuttaa pankkitililtä rahan nosto
		if (ottoMaara > saldo) {
			System.out.println("Saldosi ei riitä");
		}
		else if (ottoMaara < 0) {
			System.out.println("Nostettavan määrän täytyy olla positiivinen");
		}
		else {
			this.saldo = (saldo  - ottoMaara);
		}
	}
	
	
	public void talletus(double talletusMaara) {
		//talletusmetodilla voimme tallettaa rahaa tilille
		if (talletusMaara < 0) {
			System.out.println("Talletusmäärän täytyy olla positiivinen");
		}
		else {
			this.saldo = (saldo  + talletusMaara);
		}
	}

	
	public void tulostus() {
		//tämä metodi tulostaa tilin tiedot
		System.out.println("Tilin omistaja: " + nimi);
		System.out.println("Tilinumero: " + pankkiTiliNro);
		System.out.println("Tilin saldo: " + saldo + " €");
	}


	@Override
	public String toString() {
		return "Pankkitili [nimi=" + nimi + ", pankkiTiliNro=" + pankkiTiliNro + ", saldo=" + saldo + "]";
	}
			
		
	
	
	}

