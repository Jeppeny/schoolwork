
public class Subscription {
	private String lehden_nimi;
	private String tilaajan_nimi;
	private String toimitusOsoite;
	private double kuukausiHinta;
	
	public Subscription() {
		this(null, null, null, 0);
	}
	
	public Subscription(String lNimi, String tNimi, String tOsoite, double kHinta) {
		this.lehden_nimi = lNimi;
		this.tilaajan_nimi = tNimi;
		this.toimitusOsoite = tOsoite;
		this.kuukausiHinta = kHinta;
	}

	public String getLehden_nimi() {
		return lehden_nimi;
	}

	public void setLehden_nimi(String lehden_nimi) {
		this.lehden_nimi = lehden_nimi;
	}

	public String getTilaajan_nimi() {
		return tilaajan_nimi;
	}

	public void setTilaajan_nimi(String tilaajan_nimi) {
		this.tilaajan_nimi = tilaajan_nimi;
	}

	public String getToimitusOsoite() {
		return toimitusOsoite;
	}

	public void setToimitusOsoite(String toimitusOsoite) {
		this.toimitusOsoite = toimitusOsoite;
	}

	public double getKuukausiHinta() {
		return kuukausiHinta;
	}

	public void setKuukausiHinta(double kuukausiHinta) {
		this.kuukausiHinta = kuukausiHinta;
	}
	
	public void printInvoice() {
		System.out.println("Lehden nimi: " + lehden_nimi);
		System.out.println("Tilaajan nimi: " + tilaajan_nimi);
		System.out.println("Toimitusosoite: " + toimitusOsoite);
	}
	
	
}

