
public class RegularSubscription extends Subscription {
	
	private int tilauksenKesto;
	
	public RegularSubscription() {
		this(null, null, null, 0, 0);
	}
	
	public RegularSubscription(String lNimi, String tNimi, String tOsoite, double kHinta, int tKesto) {
		super(lNimi, tNimi, tOsoite, kHinta);
		this.tilauksenKesto = tKesto;
		
	}

	public int getTilauksenKesto() {
		return tilauksenKesto;
	}

	public void setTilauksenKesto(int tilauksenKesto) {
		this.tilauksenKesto = tilauksenKesto;
	}
	
	public void printInvoice() {
		System.out.println("Tilauksen tyyppi on normaalitilaus\n");
		super.printInvoice();
		System.out.println("Tilauksen kesto on: " + tilauksenKesto + " kuukautta");
		System.out.println("Tilauksen hinta on: " + getKuukausiHinta() * tilauksenKesto + " euroa");
	}
}
