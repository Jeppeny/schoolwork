
public class StandingSubscription extends Subscription {
	
	private int alennusProsentti;
	
	public StandingSubscription() {
		this(null, null, null, 0, 0);
	}

	public StandingSubscription(String lNimi, String tNimi, String tOsoite, double kHinta, int aProsentti) {
		super(lNimi, tNimi, tOsoite, kHinta);
		this.alennusProsentti = aProsentti;
	}

	public int getAlennusProsentti() {
		return alennusProsentti;
	}

	public void setAlennusProsentti(int alennusProsentti) {
		this.alennusProsentti = alennusProsentti;
	}
	
	public void printInvoice() {
		System.out.println("Tilauksen tyyppi on kestotilaus\n");
		super.printInvoice();
		System.out.println("Tilauksen kesto on: 12 kuukautta");
		System.out.println("Tilauksen hinta on: " + 12 * getKuukausiHinta() * (1 - (alennusProsentti * 0.01)) + " euroa");
	}
}
