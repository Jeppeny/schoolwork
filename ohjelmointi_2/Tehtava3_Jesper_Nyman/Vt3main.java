import java.util.Scanner;

public class Vt3main {

	public static void main(String [] args) {
		String n_LNimi;
		String k_LNimi;
		String n_TNimi;
		String k_TNimi;
		String n_TOsoite;
		String k_TOsoite;
		double n_THinta;
		double k_THinta;
		int k_AProsentti;
		int n_TKesto;
		
		System.out.println("Anna normaalitilauksen tiedot\n");
		System.out.println("Anna lehden nimi > ");
		n_LNimi = sc.nextLine();
		System.out.println("Anna tilaajan nimi > ");
		n_TNimi = sc.nextLine();
		System.out.println("Anna toimitusosoite > ");
		n_TOsoite = sc.nextLine();
		
		System.out.println("Anna tilauksen kuukausihinta euroina > ");
		while(true) {
			n_THinta = sc.nextDouble();
			if (n_THinta <= 0) {
				System.out.println("Hinnan tulee olla positiivinen, kokeile uudestaan > ");
			}
			else {
				break;
			}
		}
		
		System.out.println("Anna tilauksen kesto kuukausina > ");		
		while(true) {
			n_TKesto = sc.nextInt();
			if (n_TKesto <= 0) {
				System.out.println("Keston täytyy olla positiivinen, kokeile uudestaan > ");
			}
			else {
				break;
			}
		}
		sc.nextLine();
		System.out.println();
		
		Subscription normaaliTilaus = new RegularSubscription(n_LNimi, n_TNimi, n_TOsoite, n_THinta, n_TKesto);
		
		System.out.println("Anna kestotilauksen tiedot\n");
		System.out.println("Anna lehden nimi > ");
		k_LNimi = sc.nextLine();
		System.out.println("Anna tilaajan nimi > ");
		k_TNimi = sc.nextLine();
		System.out.println("Anna toimitusosoite > ");
		k_TOsoite = sc.nextLine();
		
		System.out.println("Anna tilauksen kuukausihinta euroina > ");
		while(true) {
			k_THinta = sc.nextDouble();
			if (k_THinta <= 0) {
				System.out.println("Hinnan tulee olla positiivinen, kokeile uudestaan > ");
			}
			else {
				break;
			}
		}
		
		System.out.println("Anna alennusprosentti > ");
		while(true) {
			k_AProsentti = sc.nextInt();
			if (k_AProsentti < 0) {
				System.out.println("Alennusprosentin tulee olla epänegatiivinen, kokeile uudestaan > ");
			}
			else {
				break;
			}
		}
		
		Subscription kestoTilaus = new StandingSubscription(k_LNimi, k_TNimi, k_TOsoite, k_THinta, k_AProsentti);
		
		printSubscriptionInvoice(normaaliTilaus);
		System.out.println();
		printSubscriptionInvoice(kestoTilaus);
	}
	
	static void printSubscriptionInvoice(Subscription subs) {
		subs.printInvoice();
	}
	
	public final static Scanner sc = new Scanner(System.in);
}
