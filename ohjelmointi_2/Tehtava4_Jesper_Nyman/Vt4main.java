import java.util.Scanner;

public class Vt4main {

	public static void main (String [] args) {
		
		InsInfoContainer infoCont1 = new InsInfoContainer();
		
		InsuranceInfo i1 = new InsuranceInfo("asunto", "oulu", 60000);
		InsuranceInfo i2 = new InsuranceInfo("omakotitalo", "vaasa", 200000);
		InsuranceInfo i3 = new InsuranceInfo("omakotitalo", "helsinki", 500000);
		InsuranceInfo i4 = new InsuranceInfo("rivitalo", "kouvola", 80000);
		InsuranceInfo i5 = new InsuranceInfo("luhtitalo", "utajärvi", 40000);
		
		infoCont1.lisaaTiedot(i1);
		infoCont1.lisaaTiedot(i2);
		infoCont1.lisaaTiedot(i3);
		infoCont1.lisaaTiedot(i4);
		infoCont1.lisaaTiedot(i5);
		
		infoCont1.tulostaKaikki();
		
		System.out.println("Valitse raja, jota halvempien kiinteistöjen tiedot tulostetaan > ");
		while(true) {
			float arvo1 = sc.nextFloat();
			if(arvo1 <= 0) {
				System.out.println("Valitun rajan tulee olla positiivinen luku! Yritä uudestaan > ");
			}
			else {
				infoCont1.tulostaPienempi(arvo1);
				break;
			}
		}
		

		System.out.println("Valitse raja, jota kalliimpien kiinteistöjen tiedot tulostetaan > ");
		
		while(true) {
			float arvo2 = sc.nextFloat();
			if(arvo2 < 0) {
				System.out.println("Valitun rajan tulee olla epänegatiivinen luku! Yritä uudestaan > ");
			}
			else {
				infoCont1.tulostaSuurempi(arvo2);
				break;
			}
		}
		
		
	}
	
	public final static Scanner sc = new Scanner(System.in);
}
