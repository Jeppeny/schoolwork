
public class InsuranceInfo {

	private Property property;
	private double vakuutusArvo;
	
	public InsuranceInfo(String t, String s, double v) {
		this.property = new Property(t, s);
		this.vakuutusArvo = v;
	}
	
	public Property getProperty() {
		return property;
	}
	public void setProperty(Property property) {
		this.property = property;
	}
	public double getVakuutusArvo() {
		return vakuutusArvo;
	}
	public void setVakuutusArvo(double vakuutusArvo) {
		this.vakuutusArvo = vakuutusArvo;
	}
	
	public void tulostaKaikkiTiedot() {
		property.tulostaTiedot();
		System.out.println("Kiinteistön vakuutusarvo: " + vakuutusArvo + "€");
	}
	
}
