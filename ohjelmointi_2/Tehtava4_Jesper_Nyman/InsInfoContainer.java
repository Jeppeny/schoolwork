import java.util.ArrayList;
import java.util.List;


public class InsInfoContainer {

	private List<InsuranceInfo> vakuutusTiedot = new ArrayList<>();

	public void lisaaTiedot(InsuranceInfo tiedot) {
		vakuutusTiedot.add(tiedot);
	}
	
	public void tulostaKaikki() {
		for(int i = 0; i < vakuutusTiedot.size(); i++) {
			InsuranceInfo info = vakuutusTiedot.get(i);
			System.out.println("Kiinteistö " + (i+1) + ":");
			info.tulostaKaikkiTiedot();
			System.out.println();
			}
	}
	
	public void tulostaSuurempi(float arvo) {
		for(int i = 0; i < vakuutusTiedot.size(); i++) {
			InsuranceInfo info = vakuutusTiedot.get(i);
			if(info.getVakuutusArvo() > arvo) {
				System.out.println("Kiinteistö " + (i+1) + ":");
				info.tulostaKaikkiTiedot();
				System.out.println();
				}

		}
	}
	public void tulostaPienempi(float arvo) {
		for(int i = 0; i < vakuutusTiedot.size(); i++) {
			InsuranceInfo info = vakuutusTiedot.get(i);
			if(info.getVakuutusArvo() < arvo) {
				System.out.println("Kiinteistö " + (i+1) + ":");
				info.tulostaKaikkiTiedot();
				System.out.println();
				}

		}
	}
	
	
}
