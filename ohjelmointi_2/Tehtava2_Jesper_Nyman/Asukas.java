
public class Asukas {
	//Tontilla sijaitsevan rakennuksen asukasta mallintava luokka
	
	private String nimi;
	private String syntymaAika;
	
	public Asukas() {
		this(null, null);
	}
	public Asukas(String nimi, String syntymaAika) {
		this.nimi = nimi;
		this.syntymaAika = syntymaAika;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getSyntymaAika() {
		return syntymaAika;
	}
	public void setSyntymaAika(String syntymaAika) {
		this.syntymaAika = syntymaAika;
	}


	@Override
	public String toString() {
		return "Asukas [nimi=" + nimi + ", syntymaAika=" + syntymaAika + "]";
	}

	public void tulostus() {
		System.out.println("Asukkaan nimi on " + nimi);
		System.out.println("Asukkaan syntymäpäivä on " + syntymaAika);
	}
}
