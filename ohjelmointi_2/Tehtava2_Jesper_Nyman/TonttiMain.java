public class TonttiMain {
	
	public static void main(String [] args) {
		
		Tontti tontti = new Tontti();
		
		System.out.println("Anna tontin nimi > ");
		tontti.setNimi(tontti.lukija.nextLine());
		
		System.out.println("Anna tontin pituusaste > ");
		tontti.setPituus(tontti.lukija.nextLine());
		
		System.out.println("Anna tontin leveysaste > ");
		tontti.setLeveys(tontti.lukija.nextLine());

		System.out.println("Anna tontin pinta-ala > ");
		
		while(true) {
			double pintaAla = tontti.lukija.nextDouble();
			if (pintaAla <= 0) {
				System.out.println("Pinta-alan tulee olla positiivinen, kokeile uudestaan > ");
			}
			else {
				tontti.setAla(pintaAla);
				break;
			}
		}
		
		System.out.println("Anna tontilla sijaitsevan rakennuksen pinta-ala > ");

		while(true) {
			double pintaAla = tontti.lukija.nextDouble();
			if (pintaAla <= 0) {
				System.out.println("Pinta-alan tulee olla positiivinen, kokeile uudestaan > ");
			}
			else {
				tontti.setRakennusAla(pintaAla);
				break;
			}
		}
		
		System.out.println("Anna tontilla sijaitsevan rakennuksen huoneiden lukumäärä > ");
		tontti.setRakennusHuoneLkm(tontti.lukija.nextInt());
		tontti.lukija.nextLine();
		
		System.out.println("Anna rakennuksen asukkaan nimi > ");
		tontti.setRakennusAsukas(tontti.lukija.nextLine());
		
		System.out.println("Anna rakennuksen asukkaan syntymäpäivä > ");
		tontti.setRakennusAsukasSyntymaAika(tontti.lukija.nextLine());
		
		tontti.tulostus();
		
		
	}
	
}
