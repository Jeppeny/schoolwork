
public class Rakennus {
	//Tontilla sijaitsevaa rakennusta mallintava luokka
	
	private double ala;
	private int huoneLkm;
	private Asukas asukas;
	
	public Rakennus() {
		this(0, 0);
	}
	public Rakennus(double ala, int huoneLkm) {
		this.ala = ala;
		this.huoneLkm = huoneLkm;
		this.asukas = new Asukas();	
	}
	public Double getAla() {
		return ala;
	}
	public void setAla(Double ala) {
		this.ala = ala;
	}
	public int getHuoneLkm() {
		return huoneLkm;
	}
	public void setHuoneLkm(int huoneLkm) {
		this.huoneLkm = huoneLkm;
	}
	public void setAsukasNimi(String nimi) {
		asukas.setNimi(nimi);
	}
	public void setAsukasSyntymaAika(String syntymaAika) {
		asukas.setSyntymaAika(syntymaAika);
	}
	
	@Override
	public String toString() {
		return "Rakennus [ala=" + ala + ", huoneLkm=" + huoneLkm + ", asukas=" + asukas + "]";
	}
	public void tulostus() {
		System.out.println("Rakennuksen huoneiden lukumäärä on " + huoneLkm);
		System.out.println("Rakennuksen pinta-ala on " + ala + " neliömetriä");
		asukas.tulostus();
	}	
}
