import java.util.Scanner;

public class Tontti {
	//Tonttia mallintava luokka
	
	private String leveys;
	private String pituus;
	private String nimi;
	private double ala;
	private Rakennus rakennus;
	
	public Tontti() {
		this(null, null, null, 0);
	}
	public Tontti(String leveys, String pituus, String nimi, double ala) {
		this.leveys = leveys;
		this.pituus = pituus;
		this.nimi = nimi;
		this.ala = ala;
		this.rakennus = new Rakennus();
	}
	public void setRakennusAla(double ala) {
		rakennus.setAla(ala);
	}
	public void setRakennusHuoneLkm(int huoneLkm) {
		rakennus.setHuoneLkm(huoneLkm);
	}
	public void setRakennusAsukas(String nimi) {
		rakennus.setAsukasNimi(nimi);
	}
	public void setRakennusAsukasSyntymaAika(String syntymaAika) {
		rakennus.setAsukasSyntymaAika(syntymaAika);
	}
	public String getLeveys() {
		return leveys;
	}
	public void setLeveys(String leveys) {
		this.leveys = leveys;
	}
	public String getPituus() {
		return pituus;
	}
	public void setPituus(String pituus) {
		this.pituus = pituus;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public Double getAla() {
		return ala;
	}
	public void setAla(Double ala) {
		this.ala = ala;
	}

	@Override
	public String toString() {
		return "Tontti [leveys=" + leveys + ", pituus=" + pituus + ", nimi=" + nimi + ", ala=" + ala + ", rakennus="
				+ rakennus + "]";
	}

	public void tulostus() {
		System.out.println("Tontin koordinaatit ovat " + pituus +" " + leveys);
		System.out.println("Tontin nimi on " + nimi);
		System.out.println("Tontin pinta-ala on " + ala + " neliömetriä");
		rakennus.tulostus();
	}
	public final Scanner lukija = new Scanner(System.in);
	//lukijalla saamme ohjelmaan syötteen käyttäjältä
	
}
